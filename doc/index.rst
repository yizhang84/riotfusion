.. riotfusion documentation master file, created by
   sphinx-quickstart on Mon Oct  3 18:10:28 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to riotfusion's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 3

   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

