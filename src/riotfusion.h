#ifndef _RIOTFUSION_H_
#define _RIOTFUSION_H_

#include <isl/union_set.h>
#include <isl/union_map.h>

#ifdef __cplusplus
extern "C" {
#endif

#define N_ELEM(x) (sizeof(x)/sizeof(x[0]))

#define RF_VAR_NAME_LEN 64
#define RF_VAR_URI_LEN 64
	
typedef enum {INPUT, OUTPUT, TEMP} rf_var_type;
typedef enum {READ, WRITE, SCHED} rf_map_type;

typedef struct rf_program rf_program;
typedef struct rf_operator rf_operator;

rf_program *rf_program_alloc();
void rf_program_free(rf_program *p);

void rf_program_add_variable(rf_program *prog, rf_var_type type,
			     const char *var);
void rf_program_add_operator(rf_program *prog, rf_operator *op);
rf_operator *rf_program_find_operator(rf_program *prog, const char *name);
void rf_program_print_variables(rf_program *prog);

rf_operator *rf_operator_alloc(const char *name);
void rf_operator_free(rf_operator *p);
void rf_operator_add_arg(rf_operator *p, const char *formal,
			 const char *actual);
const char* rf_operator_get_arg(rf_operator *p, const char *formal);
void rf_operator_add_param(rf_operator *p, int n, const char **param);
void rf_operator_add_domain(rf_operator *p, isl_union_set *s);
void rf_operator_add_map(rf_operator *p, rf_map_type t,
			 isl_union_map *m);
isl_ctx *rf_operator_ctx(rf_operator *p);
void rf_operator_print(rf_operator *p);

#ifdef __cplusplus
}
#endif
#endif /* _RIOTFUSION_H_ */
