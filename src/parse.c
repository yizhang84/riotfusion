#include <yaml.h>
#include <json/json.h>
#include <syslog.h>
#include <assert.h>
#include <isl/map.h>
#include "parse.h"


void parse_vars(rf_var_type type, yaml_parser_t *parser, rf_program *prog)
{
}

void parse_op_list(yaml_parser_t *parser, rf_program *prog)
{
}
/*
void parse_top_level(yaml_parser_t *parser, rf_program *prog)
{
	yaml_event_t event;
	yaml_char_t *str;
	
	yaml_parser_parse(parser, &event);
	if (event.type == YAML_MAPPING_START_EVENT) {
		yaml_event_delete(&event);
		yaml_parser_parse(parser, &event);
		assert(event.type == YAML_SCALAR_EVENT);
		str = event.data.scalar.value;
		assert(strcmp(str, "input") == 0);
		yaml_event_delete(&event);
		parse_vars(INPUT, parser, prog);
		
		yaml_parser_parse(parser, &event);
		assert(event.type == YAML_SCALAR_EVENT);
		str = event.data.scalar.value;
		assert(strcmp(str, "output") == 0);
		yaml_event_delete(&event);
		parse_vars(OUTPUT, parser, prog);
		
		yaml_parser_parse(parser, &event);
		assert(event.type == YAML_SCALAR_EVENT);
		str = event.data.scalar.value;
		assert(strcmp(str, "temp") == 0);
		yaml_event_delete(&event);
		parse_vars(TEMP, parser, prog);

		yaml_parser_parse(parser, &event);
		assert(event.type == YAML_SCALAR_EVENT);
		str = event.data.scalar.value;
		assert(strcmp(str, "program") == 0);
		yaml_event_delete(&event);
		parse_op_list(parser, prog);
	}
}
*/	
rf_program *rf_parse_program(const char *filename)
{
    rf_program *prog = rf_program_alloc();
	
    json_object *top = json_object_from_file((char*)filename);
    syslog(LOG_DEBUG, "json top level type = %d\n", json_object_get_type(top));
    {
	json_object *input = json_object_object_get(top, "input");
	/* syslog(LOG_DEBUG, "%s", json_object_to_json_string(input)); */
	json_object_object_foreach(input, key, val) {
	    syslog(LOG_DEBUG, "putting %s", key);
	    rf_program_add_variable(prog, INPUT, key);
	}
    }
    {
	json_object *output = json_object_object_get(top, "output");
	json_object_object_foreach(output, key, val) {
	    syslog(LOG_DEBUG, "putting %s", key);
	    rf_program_add_variable(prog, OUTPUT, key);
	}
    }
    {
	json_object *temp = json_object_object_get(top, "temp");
	json_object_object_foreach(temp, key, val) {
	    syslog(LOG_DEBUG, "putting %s", key);
	    rf_program_add_variable(prog, TEMP, key);
	}
    }
    json_object *ops = json_object_object_get(top, "program");
    array_list *array = json_object_get_array(ops);
    int n = array_list_length(array);
    for (int i=0; i<n; ++i) {
	json_object *obj = (json_object*) array_list_get_idx(array, i);
	array_list *stmt = json_object_get_array(obj);
	int m = array_list_length(stmt);
	const char *op_name_args[m];
	for (int j=0; j<m; ++j) {
	    json_object *x = (json_object*)
		array_list_get_idx(stmt, j);
	    /* operator name and args are all strings */
	    assert(json_type_string == json_object_get_type(x));
	    op_name_args[j] = json_object_get_string(x);
	}
	rf_operator *op;
	if ((op = rf_program_find_operator(prog, op_name_args[0])))
	    ;
	else
	    op = rf_parse_operator(m, op_name_args);
	/* add to program */
	rf_program_add_operator(prog, op);
    }
    json_object_put(top); /* decrement ref count */
    /* rf_program_print_variables(prog); */
    return prog;
}

rf_operator *rf_parse_operator(int nargs, const char **name_args)
{
    const char *opname = name_args[0];
    rf_operator *op = rf_operator_alloc(opname);
    char buf[100];
    strncpy(buf, opname, sizeof(buf));
    strncat(buf, ".json", sizeof(buf) - strlen(buf) - 1);
    syslog(LOG_INFO, "parsing %s", buf);
    json_object *top = json_object_from_file(buf);
    
    /* get input */
    json_object *input = json_object_object_get(top, "input");
    array_list *arr = json_object_get_array(input);
    int nn = array_list_length(arr);
    /* formal and acutal args should match */
    assert(nn == nargs-1);
    for (int i=1; i<nargs; ++i) {
	json_object *x = (json_object*) array_list_get_idx(arr, i-1);
	rf_operator_add_arg(op, json_object_get_string(x), name_args[i]);
    }
    /* todo: parse output and temp */
    
    /* get global params */
    json_object *params = json_object_object_get(top, "parameter");
    arr = json_object_get_array(params);
    nn = array_list_length(arr);
    for (int i=0; i<nn; ++i) {
	json_object *param = (json_object*) array_list_get_idx(arr, i);
	array_list *tmp = json_object_get_array(param);
	int m = array_list_length(tmp);
	assert(m > 1);
	const char *x[m];
	for (int j=0; j<m; ++j) {
	    json_object *s = (json_object*) array_list_get_idx(tmp, j);
	    x[j] = json_object_get_string(s);
	}
	rf_operator_add_param(op, m, x);
	syslog(LOG_DEBUG, "global param %s", x[0]);
    }

    /* get domain */
    arr = json_object_get_array(json_object_object_get(top, "domain"));
    nn = array_list_length(arr);
    for (int i=0; i<nn; ++i) {
	json_object *s = (json_object*) array_list_get_idx(arr, i);
	syslog(LOG_DEBUG, "domain %s", json_object_get_string(s));
	isl_union_set *set = isl_union_set_read_from_str(
	    rf_operator_ctx(op),
	    json_object_get_string(s));
	rf_operator_add_domain(op, set);
    }
    /* get write */
    arr = json_object_get_array(json_object_object_get(top, "write"));
    nn = array_list_length(arr);
    for (int i=0; i<nn; ++i) {
	json_object *s = (json_object*) array_list_get_idx(arr, i);
	syslog(LOG_DEBUG, "write %s", json_object_get_string(s));
	isl_union_map *map = isl_union_map_read_from_str(
	    rf_operator_ctx(op),
	    json_object_get_string(s));
	rf_operator_add_map(op, WRITE, map);
    }
    /* get read */
    arr = json_object_get_array(json_object_object_get(top, "read"));
    nn = array_list_length(arr);
    for (int i=0; i<nn; ++i) {
	json_object *s = (json_object*) array_list_get_idx(arr, i);
	syslog(LOG_DEBUG, "read %s", json_object_get_string(s));
	isl_union_map *map = isl_union_map_read_from_str(
	    rf_operator_ctx(op),
	    json_object_get_string(s));
	rf_operator_add_map(op, READ, map);
    }
    /* get sched */
    arr = json_object_get_array(json_object_object_get(top, "schedule"));
    nn = array_list_length(arr);
    for (int i=0; i<nn; ++i) {
	json_object *s = (json_object*) array_list_get_idx(arr, i);
	syslog(LOG_DEBUG, "sched %s", json_object_get_string(s));
	isl_union_map *map = isl_union_map_read_from_str(
	    rf_operator_ctx(op),
	    json_object_get_string(s));
	rf_operator_add_map(op, SCHED, map);
    }

    rf_operator_print(op);
    json_object_put(top); /* decrement ref count */
    return op;
}

