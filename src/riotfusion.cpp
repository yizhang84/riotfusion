#include <stdlib.h>
#include "riotfusion.h"
#include <QHash>
#include <QDebug>
#include <assert.h>

struct rf_var
{
    rf_var_type type;
    char uri[RF_VAR_NAME_LEN];
};

struct rf_operator
{
    typedef QHash<QString, QString> ArgTable;
    typedef QHash<QString, QList<QString> > ParamTable;
    ArgTable *arg_table;
    ParamTable *param_table;

    char name[RF_VAR_NAME_LEN];
    isl_ctx *ctx;
    isl_union_set *domain;
    isl_union_map *maps[3];
    /*
    isl_union_map *read;
    isl_union_map *write;
    isl_union_map *sched;
    */
};

struct rf_program
{
    typedef QHash<QString, rf_var> Hash;
    typedef QHash<QString, rf_operator*> OperatorCache;
    Hash *hash;
    OperatorCache *opcache;
};

rf_program *rf_program_alloc()
{
    rf_program *p = (rf_program*) malloc(sizeof(rf_program));
    p->hash = new rf_program::Hash;
    p->opcache = new rf_program::OperatorCache;
    return p;
}

void rf_program_free(rf_program *p)
{
    delete p->hash;
    rf_program::OperatorCache::iterator it;
    for (it = p->opcache->begin(); it != p->opcache->end(); ++it)
	rf_operator_free(it.value());
    delete p->opcache;
    free(p);
}

void rf_program_add_variable(rf_program *prog, rf_var_type type,
			     const char *varname)
{
    rf_program::Hash *hash = prog->hash;
    struct rf_var var;
    var.type = type;
    hash->insert(varname, var); 
}

void rf_program_add_operator(rf_program *prog, rf_operator *op)
{
    assert(strlen(op->name) > 0);
    rf_program::OperatorCache *cache = prog->opcache;
    if (cache->find(op->name) == cache->end())
	cache->insert(op->name, op);
    /* global parameter name replacement */
}

rf_operator *rf_program_find_operator(rf_program *prog, const char *name)
{
    rf_program::OperatorCache *cache = prog->opcache;
    rf_program::OperatorCache::iterator it = cache->find(name);
    if (it != cache->end())
	return it.value();
    return NULL;
}
    
void rf_program_print_variables(rf_program *prog)
{
    rf_program::Hash *hash = prog->hash;
    QString str;
    Q_FOREACH(str, hash->uniqueKeys()) {
	//printf("key %s\n", str);
	qDebug() << str[0];
    }
}

rf_operator *rf_operator_alloc(const char *name)
{
    rf_operator *p = (rf_operator*) malloc(sizeof(rf_operator));
    strncpy(p->name, name, sizeof(p->name)-1);
    p->name[sizeof(p->name)-1] = '\0';
    p->arg_table = new rf_operator::ArgTable;
    p->param_table = new rf_operator::ParamTable;
    p->ctx = isl_ctx_alloc();
    isl_dim *dim = isl_dim_alloc(p->ctx, 0, 0, 0);
    p->domain = isl_union_set_empty(isl_dim_copy(dim));
    int size = N_ELEM(p->maps);
    for (int i=0; i<size; ++i)
	p->maps[i] = isl_union_map_empty(isl_dim_copy(dim));
    isl_dim_free(dim);
    return p;
}

void rf_operator_free(rf_operator *p)
{
    delete p->arg_table;
    delete p->param_table;
    isl_union_set_free(p->domain);
    int size = N_ELEM(p->maps);
    for (int i=0; i<size; ++i)
	isl_union_map_free(p->maps[i]);
    isl_ctx_free(p->ctx);
    free(p);
}

void rf_operator_add_arg(rf_operator *p, const char *formal,
			 const char *actual)
{
    p->arg_table->insert(QString(formal), QString(actual));
}

const char* rf_operator_get_arg(rf_operator *p, const char *formal)
{
    return p->arg_table->value(formal).toLatin1().constData();
}

void rf_operator_add_param(rf_operator *p, int n, const char **param)
{
    QList<QString> list;
    for (int i=1; i<n; ++i)
	list << param[i];
    p->param_table->insert(param[0], list);
}

void rf_operator_add_domain(rf_operator *p, isl_union_set *s)
{
    p->domain = isl_union_set_union(p->domain, s);
}

void rf_operator_add_map(rf_operator *p, rf_map_type t,
			 isl_union_map *m)
{
    m = isl_union_map_intersect_domain(m, isl_union_set_copy(p->domain));
    p->maps[t] = isl_union_map_union(p->maps[t], m);
}

isl_ctx *rf_operator_ctx(rf_operator *p)
{
    return p->ctx;
}

void rf_operator_print(rf_operator *p)
{
    FILE *file = stdout;
    isl_printer *printer = isl_printer_to_file(p->ctx, file);
    fprintf(file, "\ndomain:\n");
    isl_printer_print_union_set(printer, p->domain);
    fprintf(file, "\nread:\n");
    isl_printer_print_union_map(printer, p->maps[READ]);
    fprintf(file, "\nwrite:\n");
    isl_printer_print_union_map(printer, p->maps[WRITE]);
    fprintf(file, "\nschedule:\n");
    isl_printer_print_union_map(printer, p->maps[SCHED]);
    fprintf(file, "\n");
}
