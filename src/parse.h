#ifndef _PARSE_H_
#define _PARSE_H_

#include "riotfusion.h"

rf_program  *rf_parse_program(const char* filename);
rf_operator *rf_parse_operator(int n, const char **name_args);

#endif /* _PARSE_H_ */
