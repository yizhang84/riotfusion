#include <stdio.h>
#include <syslog.h>

#include "yaml.h"
#include "parse.h"

int main(int argc, char **argv)
{
	if (argc != 2) {
		fprintf(stderr, "usage: %s program\n", argv[0]);
		exit(1);
	}

	openlog("riot", LOG_PERROR, LOG_USER);
	
	rf_program *prog = rf_parse_program(argv[1]);
	closelog();
	
}
