#!/usr/bin/env python

import unittest
import sys

sys.path.append('..')
from riotfusion.isl import *
from riotfusion.islrf import *

class TestReduceArity(unittest.TestCase):
    def setUp(self):
        self.ctx = isl_ctx_alloc()
        # islrf_path = ctypes.util.find_library("islrf")
        # self.islrf = ctypes.cdll.LoadLibrary(islrf_path)
        # self.islrf_reduce_arity = self.islrf.islrf_reduce_arity
        # self.islrf_reduce_arity.restype = POINTER(struct_isl_basic_map)
    def do_file(self, filename):
        with open(filename, 'r') as f:
            result = None
            for line in f:
                if line.startswith('##'):
                    continue
                if line.startswith('#'):
                    #self.assertTrue(result is not None)
                    if result is None:
                        continue
                    s = line[1:]
                    print s
                    ans = isl_basic_map_read_from_str(self.ctx, s, -1)
                    equal = isl_basic_map_is_equal(ans, result)
                    isl_basic_map_free(ans)
                    self.assertEqual(equal, 1)
                    isl_basic_map_free(result)
                    result = None
                else:
                    if result:
                        isl_basic_map_free(result)
                        result = None
                    print line
                    input = isl_basic_map_read_from_str(self.ctx, line, -1)
                    result = islrf_reduce_arity(input)
            if result:
                isl_basic_map_free(result)
    def test_main(self):
        self.do_file('test.in')
    def tearDown(self):
        isl_ctx_free(self.ctx)

if __name__ == '__main__':
    unittest.main()
