#ifndef LOG_H
#define LOG_H

#include <stdio.h>

#ifdef NDEBUG
#define LOG(fmt, ...) do {} while(0)
#else
#define LOG(fmt, ...) fprintf(stderr, "[LOG] " fmt, ##__VA_ARGS__)
#endif

#endif
