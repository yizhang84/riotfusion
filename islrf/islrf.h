#ifndef ISLRF_H
#define ISLRF_H

/* arity definition */
#define ISL_MANY_MANY 0
#define ISL_ONE_MANY 1
#define ISL_MANY_ONE 2
#define ISL_ONE_ONE 3

#include <isl/map.h>
#include <cloog/isl/cloog.h>

__isl_give isl_basic_map* islrf_reduce_arity(__isl_take isl_basic_map
	*bmap);
int islrf_check_arity(__isl_keep isl_basic_map *bmap);
__isl_give isl_basic_map* islrf_basic_map_from_map(__isl_keep isl_map *map);

char *islrf_codegen(__isl_take isl_union_map *umap,
                    __isl_take isl_point *point);
void islrf_free(char *p);

int islrf_union_map_compute_flow(__isl_take isl_union_map *sink,
	__isl_take isl_union_map *must_source,
	__isl_take isl_union_map *may_source,
	__isl_take isl_union_map *schedule,
	__isl_give isl_union_map **must_dep, __isl_give isl_union_map **may_dep,
	__isl_give isl_union_map **must_no_source,
	__isl_give isl_union_map **may_no_source);
#endif
