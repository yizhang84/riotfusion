#include <stdio.h>
#include <assert.h>
#include <isl/set.h>
#include <barvinok/isl.h>
#include "islrf.h"
#include "log.h"

#ifdef NDEBUG
#define islrf_mat_dump(x) do {} while (0)
#else
#define islrf_mat_dump isl_mat_dump
#endif

void islrf_print_basic_map(__isl_keep isl_basic_map *bmap)
{
    isl_ctx *ctx = isl_basic_map_get_ctx(bmap);
    isl_printer *p = isl_printer_to_file(ctx, stdout);
    p = isl_printer_print_basic_map(p, bmap);
    p = isl_printer_flush(p);
    fprintf(stdout, "\n");
    isl_printer_free(p);
}

void islrf_print_map(__isl_keep isl_map *map)
{
    isl_ctx *ctx = isl_map_get_ctx(map);
    isl_printer *p = isl_printer_to_file(ctx, stdout);
    p = isl_printer_print_map(p, map);
    p = isl_printer_flush(p);
    fprintf(stdout, "\n");
    isl_printer_free(p);
}

void islrf_print_union_map(__isl_keep isl_union_map *union_map)
{
    isl_ctx *ctx = isl_union_map_get_ctx(union_map);
    isl_printer *p = isl_printer_to_file(ctx, stdout);
    p = isl_printer_print_union_map(p, union_map);
    p = isl_printer_flush(p);
    fprintf(stdout, "\n");
    isl_printer_free(p);
}

int islrf_check_arity(__isl_keep isl_basic_map *bmap)
{
    int ret = 0;
    isl_map *temp = isl_map_from_basic_map(isl_basic_map_copy(bmap));
    if (isl_map_is_injective(temp))
	ret += ISL_ONE_MANY;
    if (isl_map_is_single_valued(temp))
	ret += ISL_MANY_ONE;
    isl_map_free(temp);
    return ret;
}

int islrf_return_basic_map(__isl_take isl_basic_map *bmap, void *user)
{
    isl_basic_map **ret = (isl_basic_map **)user;
    *ret = bmap;
    return -1; // signal iterator to stop
}

__isl_give isl_basic_map* islrf_basic_map_from_map(__isl_take isl_map *map)
{
    isl_basic_map *bmap;
    isl_map_foreach_basic_map(map, islrf_return_basic_map, &bmap);
    isl_map_free(map);
    return bmap;
}

__isl_give isl_basic_map *islrf_handle_one_many(__isl_take isl_basic_map *bmap,
    int type)
{
    //TODO: if one-many then use lexmin(many); if many-one then use
    //lexmax(many);
    assert(type == ISL_MANY_ONE || type == ISL_ONE_MANY);
    if (type == ISL_MANY_ONE)
	bmap = isl_basic_map_reverse(bmap);
    isl_map *mmin = isl_basic_map_lexmin(bmap);
    bmap = islrf_basic_map_from_map(mmin);
    if (type == ISL_MANY_ONE)
	bmap = isl_basic_map_reverse(bmap);
    return bmap;
}

/* return -1 if unrelated, index of row if related */
int islrf_related_via(__isl_keep isl_mat *mat)
{
    int nrow = isl_mat_rows(mat);
    isl_int v1,v2;
    isl_int_init(v1);
    isl_int_init(v2);
    int i;
    int ret = -1;
    for (i=0; i<nrow; ++i) {
        isl_mat_get_element(mat, i, 0, &v1);
        isl_mat_get_element(mat, i, 1, &v2);
        if (!isl_int_is_zero(v1) && !isl_int_is_zero(v2)) {
            ret = i;
            break;
        }
    }
    isl_int_clear(v1);
    isl_int_clear(v2);
    return ret;
}

/* Compute the rank of a basic set.  For now, simply return the number
   of equalities, including those under disguise.
*/

int islrf_basic_set_rank(__isl_keep isl_basic_set *bset)
{
    isl_int v,v2,sum;
    isl_int_init(v);
    isl_int_init(v2);
    isl_int_init(sum);
    int ret = isl_basic_set_n_dim(bset);
    LOG("initial rank=%d\n", ret);
    isl_mat *mat = isl_basic_set_equalities_matrix(bset, isl_dim_set,
						   isl_dim_param, isl_dim_div,
						   isl_dim_cst);
    int nrow = isl_mat_rows(mat);
    int ncol = isl_mat_cols(mat);
    int ndiv = isl_basic_set_dim(bset, isl_dim_div);
    int i,j,k;
    /* equalities with existential variables do not count */
    for (i=0; i<nrow; ++i) {
	for (j=ncol-ndiv-1; j<ncol-1; ++j) {
	    isl_mat_get_element(mat, i, j, &v);
	    if (!isl_int_is_zero(v)) {
		goto existential_equality;
	    }
	}
	ret--; /* found a real equality */
    existential_equality:
	continue;
    }
    islrf_mat_dump(mat);
    isl_mat_free(mat);
    /* find hidden equalities: i=n/3 (int division) translates into
       two inequalities, which add up to a trivial "constant >= 0"
    */
    mat = isl_basic_set_inequalities_matrix(bset, isl_dim_set,
					    isl_dim_param, isl_dim_div,
					    isl_dim_cst);
    nrow = isl_mat_rows(mat);
    ncol = isl_mat_cols(mat);
    /* i_set = isl_basic_set_dim(bset, isl_dim_set); */
    /* i_par = isl_basic_set_dim(bset, isl_dim_param) + i_set; */
    /* i_div = isl_basic_set_dim(bset, isl_dim_div) + i_par; */
    for (i=0; i<nrow-1; ++i)
	for (j=i+1; j<nrow; ++j) {
	    for (k=0; k<ncol-1; ++k) {
		isl_mat_get_element(mat, i, k, &v);
		isl_mat_get_element(mat, j, k, &v2);
		isl_int_add(sum, v, v2);
		if (!isl_int_is_zero(sum))
		    goto no_equality;
	    }
	    ret--; /* reaching here means success */
	no_equality:
	    continue;
	}
    islrf_mat_dump(mat);
    isl_mat_free(mat);
    isl_int_clear(v);
    isl_int_clear(v2);
    isl_int_clear(sum);
    LOG("rank=%d\n", ret);
    return ret;
}

int islrf_basic_map_range_rank(__isl_keep isl_basic_map *bmap)
{
    isl_basic_set *ran = isl_basic_map_range(isl_basic_map_copy(bmap));
    int ret = islrf_basic_set_rank(ran);
    isl_basic_set_free(ran);
    return ret;
}

int islrf_basic_map_domain_rank(__isl_keep isl_basic_map *bmap)
{
    isl_basic_set *dom = isl_basic_map_domain(isl_basic_map_copy(bmap));
    int ret = islrf_basic_set_rank(dom);
    isl_basic_set_free(dom);
    return ret;
}

__isl_give isl_basic_map* islrf_add_equality(__isl_take isl_basic_map *bmap,
					     int j, int i)
{
    LOG("in=%d out=%d\n", j, i);
    int k;
    int pos[2] = {j, i};
    // add a constraint: -IN[j] + OUT[i] + cst = 0
    // where cst = min(IN[0]) - min(OUT[i])
    unsigned N[] = {isl_basic_map_n_in(bmap), isl_basic_map_n_out(bmap)};
    unsigned nparam = isl_basic_map_n_param(bmap);
    isl_dim *orig_dim = isl_basic_map_get_dim(bmap);
    const char *param_name[nparam];
    for (k=0; k<nparam; ++k)
	param_name[k] = isl_dim_get_name(orig_dim, isl_dim_param, k);
    isl_int v[2];
    int row[2]; /* row index */
    isl_basic_set *proj[2]; /* domain, range after irrelevant dims pruned */
    proj[0] = isl_basic_map_domain(isl_basic_map_copy(bmap));
    proj[1] = isl_basic_map_range (isl_basic_map_copy(bmap));
    isl_mat *mat[2];
    const char *var_name[2];
    for (k=0; k<2; ++k) {
	isl_int_init(v[k]);
	/* project out dims before and after pos[k] */
	proj[k] = isl_basic_set_project_out(proj[k], isl_dim_set, pos[k]+1,
					    N[k]-pos[k]-1);
	proj[k] = isl_basic_set_project_out(proj[k], isl_dim_set, 0,
					    pos[k]);
	mat[k] = isl_basic_set_inequalities_matrix(proj[k], isl_dim_set,
						   isl_dim_param,
						   isl_dim_cst, isl_dim_div);
	assert(isl_mat_rows(mat[k])>=2); // at least lower and upper bounds
	/* Find the row with a positive coefficient.  That means a
	   constraint in the form: a x + c >=0, a>0, which denotes
	   x's lower bound.  After finding the lower bounds of
	   variable IN[j] and OUT[i], we can add an equality
	   constraint a*IN[j]-b*OUT[i]+c=0. where c is the
	   difference of the two lower bounds.*/
	for (row[k]=0; ; ++row[k]) {
	    isl_mat_get_element(mat[k], row[k], 0, &v[k]);
	    if (isl_int_is_pos(v[k]))
		break;
	}
	islrf_mat_dump(mat[k]);
	isl_dim *temp = isl_basic_set_get_dim(proj[k]);
	var_name[k] = isl_dim_get_name(temp, isl_dim_set, 0);
	isl_dim_free(temp);
	LOG("var name %s\n", var_name[k]);
    }

    if (isl_int_is_one(v[0]) && isl_int_is_one(v[1])) {
	/* there are no floor/integer fucntions involved */
	LOG("both are ones\n");
	isl_constraint *c = isl_equality_alloc(isl_basic_map_get_dim(bmap));
	isl_int vt;
	isl_int_init(vt);
	// set the coefficients for IN[0] and OUT[i]
	isl_int_neg(vt, v[0]);
	isl_constraint_set_coefficient(c, isl_dim_in, j, vt);
	isl_constraint_set_coefficient(c, isl_dim_out, i, v[1]);
	// set the coefficients for params
	for (int l=0; l<nparam; ++l) {
	    isl_mat_get_element(mat[0], row[0], l+1, &v[0]);
	    isl_mat_get_element(mat[1], row[1], l+1, &v[1]);
	    isl_int_sub(vt, v[1], v[0]);
	    isl_constraint_set_coefficient(c, isl_dim_param, l, vt);
	}
	// set the constant
	isl_mat_get_element(mat[0], row[0], nparam+1, &v[0]);
	isl_mat_get_element(mat[1], row[1], nparam+1, &v[1]);
	isl_int_sub(vt, v[1], v[0]);
	isl_constraint_set_constant(c, vt);
	
	// and we're done!
	bmap = isl_basic_map_add_constraint(bmap, c);

	// clean up
	isl_int_clear(vt);
    }
    else {
	/* k*i-n+(k-1)>=0 signifies i>=[n/k] */
	int param_coef[2][nparam];
	char buf[2][64];
	isl_int vt,vcst;
	isl_int_init(vt);
	isl_int_init(vcst);
	for (k=0; k<2; ++k) {
	    /* get constant */
	    isl_mat_get_element(mat[k], row[k], nparam+1, &vcst);
	    isl_int_sub(vt, v[k], vcst);
	    assert(isl_int_is_one(vt)); /* k-(k-1) is one */
	    int div = isl_int_get_si(v[k]); /* denominator */
	    strcpy(buf[k], "[(0");
	    char temp[64];
	    for (int l=0; l<nparam; ++l) {
		isl_mat_get_element(mat[k], row[k], 1+l, &vt);
		int d = param_coef[k][l] = - isl_int_get_si(vt);
		sprintf(temp, "%+d%s", d, param_name[l]);
		strcat(buf[k], temp);
	    }
	    sprintf(temp, ")/%d]", div);
	    strcat(buf[k], temp);
	    LOG("%s\n", buf[k]);
	}
	char constraint[128];
	sprintf(constraint, "%s-%s=%s-%s", var_name[0], var_name[1],
		buf[0], buf[1]);
	LOG("%s\n", constraint);
	/* Because it is quite involved to translate a constraint with
	 * two integer functions into a matrix form, we use isl's
	 * built-in parsing function to construct such a constraint
	 * and then intersect with the original basic map. */
	isl_basic_map *template =
	    isl_basic_map_universe(isl_dim_copy(orig_dim));
	isl_ctx *ctx = isl_basic_map_get_ctx(bmap);
	isl_printer *prn = isl_printer_to_str(ctx);
	prn = isl_printer_print_basic_map(prn, template);
	char str[256];
	strcpy(str, isl_printer_get_str(prn)); /* str looks like  "...}" */
	strcpy(str+strlen(str)-1, constraint);
	strcat(str, "}");
	LOG("%s\n", str);
	isl_printer_free(prn);
	isl_basic_map_free(template);
	template = isl_basic_map_read_from_str(ctx, str, nparam);
	bmap = isl_basic_map_intersect(bmap, template);
    }

    for (k=0; k<2; ++k) {
	isl_int_clear(v[k]);
	isl_mat_free(mat[k]);
	isl_basic_set_free(proj[k]);
    }
    isl_dim_free(orig_dim);
    return bmap;
}

__isl_give isl_basic_map* islrf_reduce_arity(__isl_take isl_basic_map *bmap)
{
    unsigned nin = isl_basic_map_n_in(bmap),
             nout = isl_basic_map_n_out(bmap),
             nparam = isl_basic_map_n_param(bmap);
    int rank_in = islrf_basic_map_domain_rank(bmap);
    int rank_out = islrf_basic_map_range_rank(bmap);
    int rank = rank_in < rank_out ? rank_in : rank_out;
    LOG("rank of in = %d\n", rank_in);
    LOG("rank of out = %d\n", rank_out);
    /* WARN: dim indexes change after a projection! */
    // check if all IN uniquely determines a OUT
    unsigned i,j;
    for (i=0; i<nout; ++i) {
        int type = islrf_check_arity(bmap);
        if (type == ISL_ONE_ONE)
            break;
        if (type == ISL_ONE_MANY || type == ISL_MANY_ONE) {
            bmap = islrf_handle_one_many(bmap, type);
            break;
        }
        // process many-many case
        isl_basic_map *temp = isl_basic_map_copy(bmap);
        temp = isl_basic_map_project_out(temp, isl_dim_out, i+1, nout-i-1);
        temp = isl_basic_map_project_out(temp, isl_dim_out, 0, i);
        isl_map *mtemp = isl_map_from_basic_map(isl_basic_map_copy(temp));
        if (isl_map_is_single_valued(mtemp)) {
            isl_map_free(mtemp);
            isl_basic_map_free(temp);
            continue;
        }
        isl_map_free(mtemp);
        // pick any dim j in IN, check its relationship with the target dim
        // (i.e., the i-th in OUT)
        int found_relation = 0;
        for (j=0; j<nin; ++j) {
            isl_basic_map *t1 = isl_basic_map_copy(temp);
            t1 = isl_basic_map_project_out(t1, isl_dim_in, j+1, nin-j-1);
            t1 = isl_basic_map_project_out(t1, isl_dim_in, 0, j);
            // now we have map IN[j] -> OUT[i]
            isl_mat *mat = isl_basic_map_equalities_matrix(t1,
                    isl_dim_in, isl_dim_out, isl_dim_param, 
                    isl_dim_cst, isl_dim_div);
            isl_basic_map_free(t1);
            int the_row;
            if ((the_row = islrf_related_via(mat)) != -1) {
                // change the_row-th  constraint into an equality to achieve
                // single-valued-ness.  Actually, add it as an extra constraint
                // to the original map.
                isl_int v1,v2;
                isl_int_init(v1);
                isl_int_init(v2);
                isl_constraint *c = isl_equality_alloc(
                        isl_basic_map_get_dim(bmap));
                isl_constraint_set_coefficient(c, isl_dim_in, j, v1);
                isl_constraint_set_coefficient(c, isl_dim_out, i, v2);
                for (int l=0; l<nparam; ++l) {
                    isl_mat_get_element(mat, the_row, 2+l, &v1);
                    isl_constraint_set_coefficient(c, isl_dim_param, l, v1);
                }
                isl_mat_get_element(mat, the_row, 2+nparam, &v1);
                isl_constraint_set_constant(c, v1);
                bmap = isl_basic_map_add_constraint(bmap, c);
                isl_int_clear(v1);
                isl_int_clear(v2);
                isl_mat_free(mat);
                found_relation = 1;
                break;
            }
            isl_mat_free(mat);
        } // for j in IN
        if (!found_relation) {
	    /* try add an equality constraint on IN[j] and OUT[i] */
	    for (j=0; j<nin; ++j) {
		LOG("try IN[%d]\n", j);
		isl_basic_map *ret =
		    islrf_add_equality(isl_basic_map_copy(bmap), j, i);
		if (islrf_basic_map_domain_rank(ret) >= rank
		    && islrf_basic_map_range_rank(ret) >= rank) {
		    isl_basic_map_free(bmap);
		    bmap = ret;
		    break;
		}
		else
		    isl_basic_map_free(ret);
	    }
	} /* end if (!found_relation */
        isl_basic_map_free(temp);
    }

    /* It may happen that dim(IN) > dim(OUT) */
    int type = islrf_check_arity(bmap);
    if (type == ISL_MANY_ONE)
	bmap = islrf_handle_one_many(bmap, type);

    return bmap;

            
    /*
    // add extra param dimension for the constant term of the hyperplane
    // i.e., make d a param dimension for cx+d=0
    bmap = isl_basic_map_add(bmap, isl_dim_param, 1); 
    // added to the end of existing params

    isl_mat *mat = isl_basic_map_equalities_matrix(bmap,
            isl_dim_in, isl_dim_out, isl_dim_param, isl_dim_cst, isl_dim_div);
    int nrow = isl_mat_rows(mat);
    assert(nrow > 0);
    int ncol = isl_mat_cols(mat);
    unsigned nin = isl_basic_map_n_in(bmap),
             nout = isl_basic_map_n_out(bmap),
             nparam = isl_basic_map_n_param(bmap);
    int i,j;
    for (i=0; i<nrow; ++i) {
        for (j=0; j<ncol; ++j) {
            isl_int v;
            isl_int_init(v);
            isl_mat_get_element(mat, i, j, &v);
            isl_int_print(stdout, v, 4);
        }
        printf("\n");
    }
    return 0;
    isl_pw_qpolynomial *card = isl_basic_map_card(bmap);
    isl_printer *p = isl_printer_to_file(ctx, stdout);
    p = isl_printer_print_pw_qpolynomial(p, card);
    isl_printer_free(p);
    */
}
