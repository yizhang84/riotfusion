#include <stdio.h>
#include "islrf.h"

/* Add constraints to set so that all params are > 0 */
static __isl_give isl_set *islrf_set_pos_param(__isl_take isl_dim *dim)
{
    isl_set *set = isl_set_universe(dim);
    int n_param = isl_set_n_param(set);
    int i;
    isl_int v, vc;
    isl_int_init(v);
    isl_int_init(vc);
    isl_int_set_si(v, 1);
    isl_int_set_si(vc, -1);
    isl_constraint *c;
    for (i=0; i<n_param; ++i) {
        c = isl_inequality_alloc(isl_set_get_dim(set));
        isl_constraint_set_coefficient(c, isl_dim_param, i, v);
        isl_constraint_set_constant(c, vc);
        isl_set_add_constraint(set, c);
    }
    isl_int_clear(v);
    isl_int_clear(vc);
    return set;
}

static __isl_give isl_set *islrf_set_from_point(__isl_take isl_dim *dim,
                                                __isl_take isl_point *point)
{
    isl_dim_free(dim);
    return isl_set_from_point(point);
    
    isl_set *set = isl_set_universe(dim);
    int n_param = isl_set_n_param(set);
    int i;
    isl_int v, vc;
    isl_int_init(v);
    isl_int_init(vc);
    isl_int_set_si(v, -1);
    isl_constraint *c;
    for (i=0; i<n_param; ++i) {
        c = isl_equality_alloc(isl_set_get_dim(set));
        isl_point_get_coordinate(point, isl_dim_param, i, &vc);
        isl_constraint_set_coefficient(c, isl_dim_param, i, v);
        isl_constraint_set_constant(c, vc);
        isl_set_add_constraint(set, c);
    }
    isl_int_clear(v);
    isl_int_clear(vc);
    isl_point_free(point);
    return set;
}

char *islrf_codegen(__isl_take isl_union_map *umap,
                    __isl_take isl_point *point)
{
    isl_ctx *ctx = isl_union_map_get_ctx(umap);
    CloogState *state;
    CloogOptions *options;
    CloogDomain *context;
    CloogUnionDomain *ud;
    CloogInput *input;
    struct clast_stmt *stmt;

    state = cloog_isl_state_malloc(ctx);
    options = cloog_options_malloc(state);
    options->language = LANGUAGE_C;
    options->strides = 1;
    options->sh = 1;

    isl_set *dom;
    isl_dim *dim = isl_union_map_get_dim(umap);
    /* If given the global param values, use them as the domain; otherwise
     * assume them to be all > 0.
     */
    if (point) {
        assert(isl_dim_equal(dim, isl_point_get_dim(point)));
        dom = isl_set_from_point(point);
        isl_dim_free(dim);
    }
    else {
        dom = islrf_set_pos_param(dim);
    }

    context = cloog_domain_from_isl_set(dom);
    ud = cloog_union_domain_from_isl_union_map(umap); /* umap consumed */
    input = cloog_input_alloc(context, ud);

    stmt = cloog_clast_create_from_input(input, options);
    
    /* use string stream to retrive textual repr of the clast */
    char *buf;
    size_t size;
    FILE *out = open_memstream(&buf, &size);
    clast_pprint(out, stmt, 0, options);
    fclose(out);
    cloog_clast_free(stmt);
    //cloog_input_free(input);
    //cloog_domain_free(context);
    //cloog_union_domain_free(ud);

    cloog_options_free(options);
    cloog_state_free(state);

    return buf;
}

void islrf_free(char *p)
{
    free(p);
}
