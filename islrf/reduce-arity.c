#include <stdio.h>
#include <assert.h>
#include <isl/set.h>
#include <barvinok/isl.h>
#include "islrf.h"

isl_printer *p;

void print_basic_map(__isl_keep isl_basic_map *bmap)
{
    p = isl_printer_print_basic_map(p, bmap);
    p = isl_printer_flush(p);
}

void print_basic_map_mat(__isl_keep isl_basic_map *bmap)
{
    isl_mat *mat = isl_basic_map_equalities_matrix(bmap, isl_dim_in,
						   isl_dim_out, isl_dim_param,
						   isl_dim_div, isl_dim_cst);
    int nrow, ncol, i, j;
    isl_int v;
    isl_int_init(v);
    nrow = isl_mat_rows(mat);
    ncol = isl_mat_cols(mat);
    isl_mat_dump(mat);
    for (i=0; i<nrow; ++i) {
	for (j=0; j<ncol; ++j) {
	}
    }
    isl_mat_free(mat);
    isl_int_clear(v);

    mat = isl_basic_map_inequalities_matrix(bmap, isl_dim_in,
					    isl_dim_out, isl_dim_param,
					    isl_dim_div, isl_dim_cst);
    isl_mat_dump(mat);
    isl_mat_free(mat);
}

int main()
{
    isl_ctx *ctx = isl_ctx_alloc();

    //const char *S = "[n]->{a[i,j,k]->b[l,m]:0<=i,j,k,l,m<n and i=l and j+k=m}";
    // non-unit stride: 0,2,4,...,[n/2]*2
    //const char *S = "[n]->{[i]->[j]:exists a: j=2*a and 0<=i,j<n}";
    // bound contains floor function: 0,1,2,...,[n/2]
    const char *S = "[n]->{[i]->[j]:j=[n/3] and 0<=i,j<n}";
    isl_basic_map *x = isl_basic_map_read_from_str(ctx, S, 1);
    //print_basic_map_mat(x);
    isl_basic_map_free(x);
    
    p = isl_printer_to_file(ctx, stdout);
    char buf[512];
    printf("[in]: ");
    while (fgets(buf, 512, stdin)) {
	if (buf[0] == '#')
	    continue;
        isl_basic_map *bmap = isl_basic_map_read_from_str(ctx, buf, 1);
        p = isl_printer_print_basic_map(p, bmap);
        printf("\n[out]:");
        bmap = islrf_reduce_arity(bmap);
        p = isl_printer_print_basic_map(p, bmap);
        printf("\n\n[in]: ");
        isl_basic_map_free(bmap);
    }
    printf("\nbye\n");
    isl_printer_free(p);
    isl_ctx_free(ctx);
}
/*
int main()
{
    const char *S = "[n,c]->{[i,j,ii,jj]:0<=i,ii,jj<=n and 0<=j<=i and i-j=ii and i+jj+c=0 and n>0}";
    isl_ctx *ctx = isl_ctx_alloc();
    isl_basic_set *bset = isl_basic_set_read_from_str(ctx, S, 2);
    isl_pw_qpolynomial *card = isl_basic_set_card(bset);
    int tight;
    isl_printer *p = isl_printer_to_file(ctx, stdout);
    p = isl_printer_print_pw_qpolynomial(p, card);
    printf("\n");
    isl_pw_qpolynomial_fold *fold = isl_pw_qpolynomial_bound(card,
            isl_fold_max, &tight);
    p = isl_printer_print_pw_qpolynomial_fold(p, fold);
    printf("\n");
    isl_printer_free(p);
}
*/
