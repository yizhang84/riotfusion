'''
Implement several parsers for RIOT programs.

Created on Oct 8, 2011

@author: yizhang
'''

import json
import yaml
import logging
from riot.util import *
from riot.fusion import *


class YamlProgramParser:
    
    FILE_SUFFIX = ".yaml"
    def __init__(self, filepath):
        """Create a YAML format program parser with the YAML file path."""
        self._file = filepath
        self._domain_id = 0
        self._eq_params = EqClass()
        self.ctx = isl_ctx_alloc()
        dim = isl_dim_alloc(self.ctx, 0, 0, 0)
        self.domain   = isl_union_set_empty(isl_dim_copy(dim))
        self.read     = isl_union_map_empty(isl_dim_copy(dim))
        self.write    = isl_union_map_empty(isl_dim_copy(dim))
        self.schedule = isl_union_map_empty(isl_dim_copy(dim))
        isl_dim_free(dim)

    def parse(self):
        with open(self._file, 'r') as f:
            try:
                top = yaml.load(f)
            except yaml.YAMLError, exc:
                if hasattr(exc, 'problem_mark'):
                    mark = exc.problem_mark
                    logging.error('error in parsing %s (%s:%s)' %
                                  (self._file, mark.line+1, mark.column+1))
        # parse variables
        var_dict = {}
        if top['temp']:
            for v in top['temp'].itervalues():
                v['type'] = 'temp'
            var_dict.update(top['temp'])
        for cat in ['input', 'output']:
            if top[cat]:
                for v in top[cat].itervalues():
                    v['type'] = 'regular'
                var_dict.update(top[cat])
            # for name in top[cat]:
            #     var = Variable(name, getattr(Variable, cat.upper()),
            #                    top[cat][name]['uri'],
            #                    top[cat][name]['size'],
            #                    top[cat][name]['block-size'])
            #     variables[name] = var
        # parse operators
        self.var_dict = VariableDict(var_dict)
        ops = top['program'] # array
        step_no = 0
        for op in ops:
            op_name = op[0]
            self._parse_op(op_name, op[1:], step_no)
            step_no += 1
        self._unify_params()
        return Program(self.ctx,
                       self.domain, self.read, self.write, self.schedule,
                       self.var_dict)
        
    def _parse_op(self, _file, actual_args, global_step_no):
        filename = _file + self.FILE_SUFFIX
        with open(find_file_in_lib(filename), 'r') as f:
            try:
                top = yaml.load(f)
            except yaml.YAMLError, exc:
                if hasattr(exc, 'problem_mark'):
                    mark = exc.problem_mark
                    logging.error('error in parsing %s (%s:%s)' %
                                  (self._file, mark.line+1, mark.column+1))
        # args/inputs
        args = top['input']
        # var_table contain global params, domain names, and array names
        var_table = dict(zip(args, actual_args))
        params = []
        # global parameters
        for x in top['parameter']:
            global_vars = []
            for local_var in x[1:]:
                name = local_var[:local_var.rfind('_')]
                global_vars.append(var_table[name] + local_var[local_var.rfind('_'):])
            self._add_equality(global_vars)
            params.append(global_vars[0])
            var_table[x[0]] = global_vars[0]
        # Construct a string for all global params of the form '[n1,...]'.
        # It will be appended to the domain spec string.
        domain_prefix = '[%s] -> ' % (','.join(params))
        # domain
        # generate unique names for each domain, and unify global param names
        def replace_name(match):
            name = match.group()
            if name in var_table:
                return var_table[name]
            else:
                return name
        name_def = re.compile('([a-zA-Z]\w*)(?:\s*\[)') # matches domain or array names
        name_ref = re.compile('[a-zA-Z]\w*') # matches domain or array names
        sched_def = re.compile('(->\s*)(\[)')
        for x in top['domain']:
            ids = name_def.findall(x)
            for _id in ids:
                var_table[_id] = self._gen_domain_id()
            x = name_ref.sub(replace_name, x)
            self._add_domain(domain_prefix + x)
        # read, write, sched
        # replace domain names and var names
        # there's no global params to be replaced because they only appear in domain spec
        for y in ['read', 'write']:
            for x in top[y]:
                x = name_ref.sub(replace_name, x)
                self._add_map(x, y)
        # for schedules, prepend a dimension, global_step_no, for global ordering
        y = 'schedule'
        for x in top[y]:
            x = name_ref.sub(replace_name, x)
            x = sched_def.sub('\\g<1> [' + str(global_step_no) + ',', x)
            self._add_map(x, y)
        
    def _unify_params(self):
        """
        Find all used global params in (domain, read, write, schedule),
        partition them according to equivalence classes as defined in
        self._eq_params, and then apply them to all the polyhedra
        """
        #TODO: we're essentially doing intersection, what about gist?
        new_s = [None]
        def do_set(s, data):
            # new_s = cast(data, POINTER(POINTER(isl_union_set)))
            n = isl_set_n_param(s)
            # map from param name to position
            pos = {isl_set_get_dim_name(s, isl_dim_param, i).data:i for i in range(n)}
            groups = self._eq_params.partition(pos.keys())
            to_project = []
            for group, representative in groups:
                eqs = list(group)
                # rename the eqs[0] to representative and let eqs[1:] be equal
                # to eqs[0]
                if eqs[0] != representative:
                    s = isl_set_set_dim_name(s, isl_dim_param, pos[eqs[0]],
                                             representative)
                for var in eqs[1:]:
                    const = gen_eq_constraint(isl_set_get_dim(s),
                                              isl_dim_param, pos[eqs[0]],
                                              isl_dim_param, pos[var])
                    s = isl_set_add_constraint(s, const)
                    to_project.append(pos[var])
            # delete large index dims first!
            for p in sorted(to_project, reverse=True):
                s = isl_set_project_out(s, isl_dim_param, p, 1)
            # assign value if dimension has size 1
            for i in range(isl_set_n_param(s)):
                name = isl_set_get_dim_name(s, isl_dim_param, i).data
                count = self.var_dict.array_size_in_blocks(name)
                if count == 1:
                    c = gen_eq_constraint(isl_set_get_dim(s),
                                          isl_dim_param, i,
                                          isl_dim_cst, count)
                    s = isl_set_add_constraint(s, c)
            # add to result
            if new_s[0]:
                new_s[0] = isl_union_set_add_set(new_s[0], s)
            else:
                new_s[0] = isl_union_set_from_set(s)
            return 0
        # for domain
        FUNC = isl_union_set_foreach_set.argtypes[1]
        old = self.domain
        isl_union_set_foreach_set(old, FUNC(do_set), None)
        isl_union_set_free(old)
        self.domain = new_s[0]
        # for read, write, schedule
        for m in ['read', 'write', 'schedule']:
            old = getattr(self, m)
            dom = isl_union_set_copy(self.domain)
            new = isl_union_map_intersect_domain(old, dom)
            setattr(self, m, new)
    
    def _gen_domain_id(self):
        """Generate a unique ID of the form "D<number>" for a new domain."""
        n = self._domain_id
        self._domain_id += 1
        return "D" + str(n)

    def _add_equality(self, var_list):
        """Add equality constraints on the given global parameters."""
        self._eq_params.add(var_list)
        
    def _add_domain(self, domain_str):
        """Add a domain polyhedron represented in a string."""
        domain = isl_union_set_read_from_str(self.ctx, domain_str)
        self.domain = isl_union_set_union(self.domain, domain)
        
    def _add_map(self, map_str, _type):
        """Add a polyhedron of _type represented in a string."""
        m = isl_union_map_read_from_str(self.ctx, map_str)
        attr = getattr(self, _type)
        setattr(self, _type, isl_union_map_union(attr, m))
