"""
Optimizes and executes RIOT programs.
"""

import logging
import subprocess
from flufl.enum import Enum
import ipdb
from riot.util import *
from riot.islrf import islrf_free
from riot.topsort import topological_sort

__docformat__ = "restructuredtext en"

logger = logging.getLogger("rf")

_MAX_STMT_ID = 10000


class Type(Enum):
    """Type of dependence."""
    RAW = 0
    WAR = 1
    WAW = 2
    RAR = 3


class Arity(Enum):
    ManyMany = 0
    OneMany = 1
    ManyOne = 2
    OneOne = 3


class Relation(Enum):
    EQ = 0
    GE = 1
    LE = 2
    # GT = 3
    # LT = 4


class Dependence(object):
    
    """
    Dependence of a program.
    """
    
    #TODO: use timestamp instead of serial ID
    
    def __init__(self, type, dep, access=None, array='', id=0):
        """Create a new Dependence object.
        
        :param dep: raw dependence polyhedron
        :type dep: isl_basic_map
        :param type: type of dependence
        :type type: :class:`Type` or str
        :param array: name of the array inducing the dependence
        :type array: str
        :param access: the array access function (target iteration -> array)
        :type access: isl_basic_map
        """
        self.id = id
        self.array_name = array
        self.dep = dep
        self.dep_reduced = islrf_reduce_arity(isl_basic_map_copy(self.dep))
        self.target_access = access
        self.source_access = isl_basic_map_apply_range(isl_basic_map_copy(dep),
                                isl_basic_map_copy(access))
        self.target_access_reduced = isl_basic_map_intersect_domain(
            isl_basic_map_copy(self.target_access),
            isl_basic_map_range(isl_basic_map_copy(self.dep_reduced)))
        self.source_access_reduced = isl_basic_map_intersect_domain(
            isl_basic_map_copy(self.source_access),
            isl_basic_map_domain(isl_basic_map_copy(self.dep_reduced)))
        self.source = int(isl_basic_map_get_tuple_name(dep, isl_dim_in).data[1:])
        self.target = int(isl_basic_map_get_tuple_name(dep, isl_dim_out).data[1:])
        if isinstance(type, str):
            type = getattr(Type, type.upper()) # convert to enum
        self.type = type
        self.arity = self._relationship()
        # arity is for the original dependence, in the following, we work on
        # a arity-reduced version
        # self.dep = islrf_reduce_arity(self.dep)
        self.is_self = self._is_self()
        self.is_real = self.type != Type.RAR
        self.sched = [None] * 2
        self.sched_cst = None
        self.weak = None
        self.strong = None
        
    def compute_schedules(self, dims=None):
        # because self/input/reuse requires a condition involving +1/-1
        # convert the basic_sets get_schedule returns into sets
        self.sched[0] = self.get_schedule(Relation.EQ, 0, True)
        self.sched[0] = isl_set_from_basic_set(self.sched[0])
        if self.is_self and self.is_real:
            self.sched[1] = self.get_schedule(Relation.EQ, 1, True)
            self.sched[1] = isl_set_from_basic_set(self.sched[1])
        elif self.is_self and not self.is_real:
            self.sched[1] = isl_basic_set_union(self.get_schedule(Relation.EQ,
                                                                  1, True),
                                                self.get_schedule(Relation.EQ,
                                                                  -1, True))
        else: # non-self
            self.sched[1] = isl_set_copy(self.sched[0])
            ctx = isl_basic_map_get_ctx(self.dep)
            if self.is_real:
                self.sched_cst_gt = True
                # s = '{[from,to]: to-from > 0}'
                # self.sched_cst = isl_set_read_from_str(ctx, s, 0)
            else:
                self.sched_cst_gt = False
                # s = '{[from,to]: to-from > 0 or to-from < 0}'
                # self.sched_cst = isl_set_read_from_str(ctx, s, 0)
        for i,x in enumerate(self.sched):
            self.sched[i] = self.align_schedule(x, dims,
                                                self.source, self.target)
        if self.is_real:
            self.weak = self.get_schedule(Relation.GE, 0)
            self.strong = self.get_schedule(Relation.GE, 1)
            self.weak = isl_set_from_basic_set(self.weak)
            self.strong = isl_set_from_basic_set(self.strong)
            if dims:
                self.weak = self.align_schedule(self.weak, dims,
                                                self.source, self.target)
                self.strong = self.align_schedule(self.strong, dims,
                                                  self.source, self.target)

    @staticmethod
    def align_schedule(sched, dims, source, target):
        ptr = 0
        for i in range(len(dims)):
            n = sum(dims[i]) + 1 # 1 for the constant dim
            if i!=source and i!=target:
                sched = isl_set_insert(sched, isl_dim_set, ptr, n)
            ptr += n
        return sched
    
    def __eq__(self, other):
        return self.id == other.id
    
    def __hash__(self):
        return hash(self.id)
    
    def __repr__(self):
        return '%s(D%d->D%d on %s %s %s)' % (self.id, self.source, self.target,
                self.array_name, self.type.enumname, self.arity.enumname)
    
    def dispose(self):
        """Free up the resources held."""
        if self.dep is None:
            return
        isl_basic_map_free(self.dep)
        isl_basic_map_free(self.dep_reduced)
        self.dep = None
        [isl_set_free(x) for x in self.sched]
        # if self.sched_cst:
        #     isl_set_free(self.sched_cst)
        if self.is_real:
            isl_set_free(self.weak)
            isl_set_free(self.strong)
        if self.target_access:
            isl_basic_map_free(self.target_access)
            isl_basic_map_free(self.source_access)
            isl_basic_map_free(self.target_access_reduced)
            isl_basic_map_free(self.source_access_reduced)
    
    def _relationship(self):
        """
        Determine the arity of this dependence
        """
        m = isl_map_from_basic_map(isl_basic_map_copy(self.dep))
        n = 0
        if isl_map_is_injective(m) == 1:
            n += int(Arity.OneMany)
        if isl_map_is_single_valued(m) == 1:
            n += int(Arity.ManyOne)
        isl_map_free(m)
        return Arity(n)
    
    def _is_self(self):
        """
        Determine if this is a self dependence
        @return: boolean result
        """
        dim = isl_basic_map_get_dim(self.dep)
        s1 = isl_dim_get_tuple_name(dim, isl_dim_in).data
        s2 = isl_dim_get_tuple_name(dim, isl_dim_out).data
        isl_dim_free(dim)
        return s1 == s2
    
    def get_schedule(self, relation=Relation.GE, const=0, reduce=False):
        """
        Compute the space of the schedule satisfying the given target-source
        relationship, i.e., sched(target) - sched(source) <relation> <const>.
        Return an isl_basic_set.
        """
        # if relation is Relation.EQ:
        #     reduced = islrf_reduce_arity(isl_basic_map_copy(self.dep))
        #     _set = self.get_coef(reduced, depth, relation, const)
        #     isl_basic_map_free(reduced)
        # else:
        if reduce:
            # dep = islrf_reduce_arity(isl_basic_map_copy(self.dep))
            ret = self.get_coef(self.dep_reduced, relation, const)
            # isl_basic_map_free(dep)
        else:
            ret = self.get_coef(self.dep, relation, const)
        return ret
        if relation is Relation.EQ and self.arity is not Arity.OneOne:
            # Consider a one (x) to many (y) dependence.  We want to compute
            # the schedule space such that for any x, there exists a y that
            # schedules of x and y satisfy such and such.  This any-exists is
            # nonlinear and too complex to handle.  As a workaround, we consider
            # two cases of the exists (y) variables: their lexmin and lexmax as
            # a function of the x.  The problem is then converted into the union
            # of two any-any subproblems, each can be solved by the affine
            # form of Farkas Lemma algorithm.
            bmap1, bmap2 = self._reduce_arity() # two isl_basic_map's
            bset1 = self.get_coef(bmap1, Relation.EQ, const)
            bset2 = self.get_coef(bmap2, Relation.EQ, const)
            isl_basic_map_free(bmap1)
            isl_basic_map_free(bmap2)
            _set = isl_basic_set_union(bset1, bset2)
            _set = isl_set_coalesce(_set)
        else:
            _set = isl_set_from_basic_set(self.get_coef(self.dep,
                                                        relation, const))
        return _set
    
    def _reduce_arity(self):
        """ Obsoleted by libislrf.so """
        def map_to_basic_map(m):
            map_to_basic_map.count = 0
            def do_basic_map(bmap, data):
                map_to_basic_map.count += 1
                map_to_basic_map.bmap = bmap
                return 0
            FUNC = isl_map_foreach_basic_map.argtypes[1]
            isl_map_foreach_basic_map(m, FUNC(do_basic_map), None)
            assert map_to_basic_map.count == 1
            return map_to_basic_map.bmap
        bmap = isl_basic_map_copy(self.dep)
        if self.arity is Arity.ManyOne:
            f = isl_basic_map_reverse  # take inverse
#            g = isl_map_reverse
        else:
            f = lambda x:x # identity function
#            g = f
        # TODO: remove this method
        bmap = f(bmap)
        m1 = isl_basic_map_lexmin(isl_basic_map_copy(bmap))
        # although m1 is isl_map, it should contain only one isl_basic_map!
        bm1 = map_to_basic_map(m1)
        bm1 = isl_basic_map_intersect(bm1, (isl_basic_map_copy(bmap)))
        bm1 = f(bm1)
        m2 = isl_basic_map_lexmax(isl_basic_map_copy(bmap))
        bm2 = map_to_basic_map(m2)
        bm2 = isl_basic_map_intersect(bm2, (isl_basic_map_copy(bmap)))
        bm2 = f(bm2)
        isl_basic_map_free(bmap)
        isl_map_free(m1)
        isl_map_free(m2)
        return (bm1, bm2)
    
    @staticmethod
    def get_coef(dep, relation=Relation.GE, const=0):
        """
        Compute the space of the coefficients of valid schedules for dep.
        
        Given a dependence polyhedron (dep), which is a map [p]->{[x]->[y]},
        compute the coefficient space {[a,b]} s.t. a[x,p] - b[y,p] >= const.
        This is called by the optimizer when computing certain multidimensional
        schedules.
        """
        #assert isinstance(dep, POINTER(isl_basic_map))
        if relation == Relation.EQ:
            x = Dependence.get_coef(dep, Relation.GE, const)
            y = Dependence.get_coef(dep, Relation.LE, const)
            return isl_basic_set_intersect(x, y)
            
        logger.debug('dep = %s', (dep))
        type_name = isl_type_name(dep)
        if type_name == 'basic_map':
            tmp = isl_basic_map_wrap(isl_basic_map_copy(dep))
            coef = isl_basic_set_coefficients(tmp)
        elif type_name == 'basic_set':
            coef = isl_basic_set_coefficients(isl_basic_set_copy(dep))
        elif type_name == 'set':
            coef = isl_set_coefficients(isl_set_copy(dep))
        else:
            raise ValueError('invalid type of dep')
        logger.debug('coef = %s', (coef))
        # coef now is a basic set with a wrapped domain: 
        # [global params] -> [D1[src domain] -> D2[target domain]]]
        dim = isl_basic_set_get_dim(coef)
        assert(isl_dim_is_wrapping(dim))
        dim = isl_dim_unwrap(dim)
        n_params = isl_dim_size(dim, isl_dim_in)
        dim = isl_dim_range(dim)
        assert(isl_dim_is_wrapping(dim))
        dim = isl_dim_unwrap(dim) # dim: [src] -> [target]
        # source and target domain names and lengths
        assert(isl_dim_size(dim, isl_dim_param) == 0)
        names = [isl_dim_get_tuple_name(dim, isl_dim_in),
                 isl_dim_get_tuple_name(dim, isl_dim_out)]
        lengths = [isl_dim_size(dim, isl_dim_in),
                   isl_dim_size(dim, isl_dim_out)]
        isl_dim_free(dim)

        # split global params to source and target parts
        if len(names) == 2:
            coef = isl_basic_set_insert(coef, isl_dim_set, n_params+lengths[0],
                                        n_params)
            mat = [None, None]
            mat[0] = isl_basic_set_equalities_matrix(coef, isl_dim_set, 
                                                   isl_dim_cst,
                                                   isl_dim_param, isl_dim_div)
            mat[1] = isl_basic_set_inequalities_matrix(coef, isl_dim_set, 
                                                     isl_dim_cst,
                                                     isl_dim_param, isl_dim_div)
            a,b,c = isl_int(), isl_int(), isl_int()
            isl_int_init(a)
            isl_int_init(b)
            isl_int_init(c)
            if relation == Relation.GE:
                start = 0
                stop = n_params + lengths[0]
            else:
                start = n_params + lengths[0]
                stop = n_params * 2 + sum(lengths)
            for k in range(2): # for equality and inequality matrices
#                isl_mat_dump(mat[k])
                nrows = isl_mat_rows(mat[k])
                for i in range(nrows):
                    # set the new target param dims
                    for j in range(0, n_params):
                        # set the new target param dims
                        isl_mat_get_element(mat[k], i, j, cast(a, isl_int_ptr))
                        mat1 = isl_mat_set_element(mat[k], i, n_params+lengths[0]+j, a)
                    # negate the source or target dims, depending on if gt==1
                    for j in range(start, stop):
                        isl_mat_get_element(mat[k], i, j, cast(a, isl_int_ptr))
                        isl_int_neg(b, a)
                        mat1 = isl_mat_set_element(mat1, i, j, b)

                    if const: # modify constant dimension
                        isl_mat_get_element(mat[k], i, 0, cast(a, isl_int_ptr))
                        isl_mat_get_element(mat[k], i, n_params*2+sum(lengths),
                                            cast(c, isl_int_ptr))
                        isl_int_set_si(cast(b, isl_int_ptr), const)
                        # If source needs to be negated ( target - source >= const)
                        # then because negation already happened (in particular,
                        # a is negated), we should use addmul.  Otheriwse, target
                        # is negated, addmul should also be used.
                        isl_int_addmul(c, a, b) # c += a*b
                        isl_mat_set_element(mat[k], i,n_params*2+sum(lengths),
                                            c)
#                isl_mat_dump(mat[k])
            isl_int_clear(a)
            isl_int_clear(b)
            isl_int_clear(c)
            bset = isl_basic_set_from_constraint_matrices(isl_basic_set_get_dim(coef),
                                                          mat[0], mat[1],
                                                          isl_dim_set, isl_dim_cst,
                                                          isl_dim_param, isl_dim_div)
            isl_basic_set_free(coef)
            coef = bset
        logger.debug('after splitting params %s', (coef))
        
        # drop duplicate dims if source and target are the same domain
        if names[0] == names[1]:
            assert(lengths[0] == lengths[1])
            stride = n_params + lengths[0]
            for v in range(stride):
                const = gen_eq_constraint(isl_basic_set_get_dim(coef),
                                           isl_dim_set, v,
                                           isl_dim_set, stride + v)
                coef = isl_basic_set_add_constraint(coef, const)
            coef = isl_basic_set_project_out(coef, isl_dim_set,
                                             stride, stride)
            logger.debug('after deduplication coef = %s', (coef))
            names.pop()
            lengths.pop()
        # rename iteration variable dims to this form: D1_<var_index>
        n = 0
        for j in range(len(names)):
            n += n_params
            for i in range(lengths[j]):
                new_name = '{}_{}'.format(names[j], i)
                coef = isl_basic_set_set_dim_name(coef, isl_dim_set,
                                                  n, new_name)
                n += 1
        # rename global params to this form: D1_<param name>
        for i in range(n_params):
            old_name = isl_basic_set_get_dim_name(coef, isl_dim_set, i).data
            new_name = '{}_{}'.format(names[0], old_name[2:])
            coef = isl_basic_set_set_dim_name(coef, isl_dim_set, i, new_name)
            if len(names) == 2:
                new_name = '{}_{}'.format(names[1], old_name[2:])
                j = n_params + lengths[0] + i
                coef = isl_basic_set_set_dim_name(coef, isl_dim_set, j, new_name)

        logger.debug('after renaming dims %s', (coef))
        logger.debug('after renaming params %s', (coef))
        return coef


class VariableDict(dict):
    """
    Table of global variables (matrix) in a riot program.
    """
    INPUT, OUTPUT, TEMP = range(3)
    def __init__(self, *args):
        super(VariableDict, self).__init__(*args)
    
    def array_size_in_blocks(self, s):
        """Return the size of an array's particular dimension as specified by s.
        For example, s='a_1' means the first dimension of array s.  The size is in
        terms of number of blocks, i.e., actual_size / block_size."""
        name, dim = s.split('_')
        dim = int(dim) - 1
        try:
            actual_size = self[name]['size'][dim]
            block_size = self[name]['block-size'][dim]
            return actual_size / block_size
        except KeyError: # key not found
            return 1
    
    def block_size(self, s):
        """Return the total volume of an array's block."""
        try:
            block_dims = self[s]['block-size']
        except KeyError:
            return 1
        vol = 1
        for d in block_dims:
            vol *= d
        return vol
    
    def array_size(self, s):
        name, dim = s.split('_')
        dim = int(dim) - 1
        return self[name]['size'][dim]


class Program(object):
    
    """
    Program represents a runnable RIOT program, composed of variable
    definitions and invocation of operators.
    
    Program is the unit of optimization.  All referenced operators are inlined
    into a global space, with local variables replaced and unified.
    """
    
    def __init__(self, ctx, domain, read, write, schedule, var_dict=None):
        self.ctx = ctx
        """The isl context; should be alive through the lifetime of Program."""
        self.deps = []
        """A list containing all dependences."""
        self.domain = domain
        """Iteration domains represented as an isl_union_set."""
        self.domain_dims = Program._domain_dims(domain)
        """List of tuples (#n_param, #n_dim) for each iteration domain."""
        self.read, self.write, self.schedule = read, write, schedule
        self.n_domain = isl_union_set_n_set(self.domain)
        """Number of iteration domains (statements) in the program."""
        self.vars = var_dict
        self.compute_dependences()
        self.pairs = Program._build_pairs(self.domain, self.domain_dims)
        """A dict from (i,j) to (lt,gt) where i and j are statement domain IDs
        and i<j, and lt (gt) is the space of coefficients such that ALL
        instances of statement j execute strictly before (after) ALL instances
        of statement i.  This is used to help separate loop nests of the
        statements in the produced schedule as much as possible so that there
        is no unnecessary interleaving and memory consumption accounting is
        accurate."""
    
    def __str__(self):
        attrs = ['domain', 'read', 'write', 'schedule']
        s = [x + ":=\t" + (getattr(self, x)) for x in attrs]
        return '\n'.join(s)
    
    def dispose(self):
        """
        Dispose external isl resources; must be called after done using program.
        """
        isl_union_set_free(self.domain)
        isl_union_map_free(self.read)
        isl_union_map_free(self.write)
        isl_union_map_free(self.schedule)
        for x in self.deps:
            x.dispose()
        for x, y in self.pairs.itervalues():
            isl_set_free(x)
            isl_set_free(y)
        isl_ctx_free(self.ctx)

    @staticmethod
    def _build_pairs(domain, domain_dims):
        n = isl_union_set_n_set(domain)
        doms = { domain_id(x) : x for x in
                isl_union_set_to_sets(isl_union_set_copy(domain))}
        pairs = dict()
        for i in range(n):
            for j in range(i+1, n):
                prod = isl_set_product(isl_set_copy(doms[i]),
                                       isl_set_copy(doms[j]))
                lt = Dependence.get_coef(prod, Relation.LE, -1)
                lt = isl_set_from_basic_set(lt)
                lt = Dependence.align_schedule(lt, domain_dims, i, j)
                gt = Dependence.get_coef(prod, Relation.GE, 1)
                gt = isl_set_from_basic_set(gt)
                gt = Dependence.align_schedule(gt, domain_dims, i, j)
                isl_map_free(prod)
                pairs[(i, j)] = (lt, gt)
        for i in doms:
            isl_set_free(doms[i])
        return pairs

    def select_deps(self, ids):
        """Return a :class:`DepSet` containing dependences with IDs as
        specified by the list `ids`."""
        return DepSet(x for x in self.deps if x.id in ids)
    
    @staticmethod
    def raw(read, write, schedule):
        # last write before read under sched
        T = (POINTER(isl_union_map))
        must_dep = T()
        ret = islrf_union_map_compute_flow(isl_union_map_copy(read),
                   isl_union_map_copy(write),
                   isl_union_map_empty(isl_union_map_get_dim(read)),
                   isl_union_map_copy(schedule),
                   byref(must_dep), None,
                   None, None)
        assert(ret >= 0)
        return must_dep
    
    @staticmethod
    def waw(read, write, schedule):
        # last write before write under sched
        T = (POINTER(isl_union_map))
        must_dep = T()
        ret = islrf_union_map_compute_flow(isl_union_map_copy(write),
                   isl_union_map_copy(write),
                   isl_union_map_empty(isl_union_map_get_dim(write)),
                   isl_union_map_copy(schedule),
                   byref(must_dep), None,
                   None, None)
        assert(ret >= 0)
        return must_dep
    
    @staticmethod
    def war(read, write, schedule):
        # any read last write before write under sched
        T = (POINTER(isl_union_map))
        may_dep = T()
        # args: sink, must, may, sched, must_dep, may_dep, must_no_src,
        # may_no_src
        ret = islrf_union_map_compute_flow(isl_union_map_copy(write),
                   isl_union_map_copy(write),
                   isl_union_map_copy(read),
                   isl_union_map_copy(schedule),
                   None, byref(may_dep),
                   None, None)
        assert(ret >= 0)
        return may_dep
    
    @staticmethod
    def rar(read, write, schedule):
        # any read last write before read under sched
        T = (POINTER(isl_union_map))
        may_dep = T()
        # args: sink, must, may, sched, must_dep, may_dep, must_no_src,
        # may_no_src
        ret = islrf_union_map_compute_flow(isl_union_map_copy(read),
                   isl_union_map_copy(write),
                   isl_union_map_copy(read),
                   isl_union_map_copy(schedule),
                   None, byref(may_dep),
                   None, None)
        assert(ret >= 0)
        return may_dep
    
#    def _depend_through_array(self, bmap, dep_type):
#        types = {'r': self.read, 'w':self.write}
#        source = types[dep_type[0]]
#        target = types[dep_type[2]]
#        sources = isl_union_map_to_basic_maps(source)
#        targets = isl_union_map_to_basic_maps(target)
#        for s in sources:
#            for t in targets:
#                # (s^-1) join bmap join t => [source access]->[target access]
#                s_r = isl_basic_map_reverse(isl_basic_map_copy(s))
#                tmp = isl_basic_map_apply_range(s_r, isl_basic_map_copy(bmap))
#                tmp = isl_basic_map_apply_range(tmp, isl_basic_map_copy(t))
#                
#                
#        copy = isl_basic_map_copy(bmap)
#        umap = isl_union_map_from_map(isl_map_from_basic_map(copy))
#        before = isl_union_map_reverse(isl_union_map_copy(self.access))
#        after = isl_union_map_copy(self.access)
#        umap = isl_union_map_apply_range(before, umap)
#        umap = isl_union_map_apply_range(umap, after)
#        name = self._find_identity_map(umap)
#        isl_union_map_free(umap)
#        return name
    
    def _build_dependence(self, bmap):
        """Given a dependence of the form D->[[S->D]->A] from source to target,
        build a :class:`Dependence` object.  bmap is consumed."""
        ran = isl_basic_map_range(isl_basic_map_copy(bmap))
        ran = isl_basic_set_unwrap(ran) # [S->D]->A for target
        sink = isl_basic_map_copy(ran)
        ran_dom = isl_basic_map_domain_map(ran) # [[S->D]->A]->[S->D]
        dep = isl_basic_map_apply_range(isl_basic_map_copy(bmap),
                                      ran_dom)  # D->[S->D]
        ran = isl_basic_map_range(isl_basic_map_copy(dep))
        ran = isl_basic_set_unwrap(ran)
        ran_ran = isl_basic_map_range_map(ran) # [S->D]->[D]
        dep = isl_basic_map_apply_range(dep, ran_ran) # D->D
        
        # sink: [S->D]->A
        dom = isl_basic_map_domain(isl_basic_map_copy(sink)) # [S->D]
        dom = isl_basic_set_unwrap(dom) # S->D
        dom_ran = isl_basic_map_range_map(dom) # [S->D]->D
        access = isl_basic_map_apply_domain(sink, dom_ran) # D->A
        array = isl_basic_map_get_tuple_name(access, isl_dim_out).data
        isl_basic_map_free(bmap)
        return (dep, access, array)
    
    @staticmethod
    def _change_stmt_id(m, dim_type, by=None, to=None):
        if by is None and to is None:
            raise ValueError("provide parameter 'delt'a or 'to'")
        if by is not None:
            dom_name = isl_func(m, 'get_tuple_name')(m, dim_type).data
            dom_id = int(dom_name[1:])
            new_dom_id = dom_id + by
        else:
            new_dom_id = to
        new_dom_name = 'D{}'.format(new_dom_id)
        return isl_func(m, 'set_tuple_name')(m, dim_type, new_dom_name)
    
    @staticmethod
    def _extend_schedule(s, n):
        """Append a constant dim with value `n` to the given schedule."""
        # isl_map_insert destroys the tuple name, but that's fine, as the map
        # is a schedule which doesn't have a tuple name for isl_out
        n_out = isl_map_n_out(s)
        s = isl_map_insert(s, isl_dim_out, n_out, 1)
        return isl_map_fix_si(s, isl_dim_out, n_out, n)
    
    def split_self_rw(self):
        """A statement may read and write to the same array element, e.g,
        a[i] += b[i], where a[i] is first read and then written.  We assume
        they are given as two separate accesses in the program specification.
        Because they correspond to the same statement and thus have the same
        schedule, we won't be able to find any dependence between them, as
        there is no notion of 'after' (as in read-after-write, for example).
        The solution is to pretend they are two separate statements with
        different schedules.  After dependence analysis is done, the two
        statements are merged back."""
        reads  = isl_union_map_to_maps(isl_union_map_copy(self.read))
        writes = isl_union_map_to_maps(isl_union_map_copy(self.write))
        scheds = isl_union_map_to_maps(isl_union_map_copy(self.schedule))
        read_dims = [isl_map_get_dim(x) for x in reads]
        write_dims = [isl_map_get_dim(x) for x in writes]
        self_rw = []
        new_read = isl_union_map_empty(isl_union_map_get_dim(self.read))
        new_sched = isl_union_map_copy(self.schedule)
        for i,r in enumerate(reads):
            for j,w in enumerate(writes):
                if (isl_dim_equal(read_dims[i], write_dims[j]) and
                    isl_map_is_equal(r, w)):
                    self_rw.append(r)
                    break
            else:
                new_read = isl_union_map_union(new_read,
                                               isl_union_map_from_map(r))
        # for each read found, add a new statement, extend its schedule and
        # also the original statement's.  Since dependence analysis doesn't
        # use self.domain, we can leave it intact and change self.read and
        # self.schedule only.
        for r in self_rw:
            # add new domain
            # dom = isl_map_domain(isl_map_copy(r))
            dom_name = isl_map_get_tuple_name(r, isl_dim_in).data
            r = self._change_stmt_id(r, isl_dim_in, by=_MAX_STMT_ID)
            new_read = isl_union_map_union(new_read,
                                           isl_union_map_from_map(r))
            # modify schedules
            for x in scheds:
                if dom_name == isl_map_get_tuple_name(x, isl_dim_in).data:
                    s = isl_map_copy(x)
                    break
            su = isl_union_map_from_map(isl_map_copy(s))
            new_sched = isl_union_map_subtract(new_sched, su)
            new_s = self._change_stmt_id(isl_map_copy(s), isl_dim_in,
                                         by=_MAX_STMT_ID)
            new_s = self._extend_schedule(new_s, 0)
            s     = self._extend_schedule(s, 1)
            new_sched = isl_union_map_union(new_sched,
                                            isl_union_map_from_map(new_s))
            new_sched = isl_union_map_union(new_sched,
                                            isl_union_map_from_map(s))
        for x in writes:
            isl_map_free(x)
        for x in scheds:
            isl_map_free(x)
        for x in write_dims:
            isl_dim_free(x)
        for x in read_dims:
            isl_dim_free(x)
        return (new_read, new_sched)
    
    @staticmethod
    def merge_self_rw(bmap):
        """The reverse of split_self_rw."""
        dom_name = isl_basic_map_get_tuple_name(bmap, isl_dim_in).data
        if dom_name[0] == 'D':
            dom_id = int(dom_name[1:])
            if dom_id >= _MAX_STMT_ID:
                bmap = Program._change_stmt_id(bmap, isl_dim_in,
                                            to=dom_id-_MAX_STMT_ID)
        ran_name = isl_basic_map_get_tuple_name(bmap, isl_dim_out).data
        if ran_name[0] == 'D':
            ran_id = int(ran_name[1:])
            if ran_id >= _MAX_STMT_ID:
                bmap = Program._change_stmt_id(bmap, isl_dim_out,
                                            to=ran_id-_MAX_STMT_ID)
        return bmap
    
    def compute_dependences(self):
        new_read, new_sched = self.split_self_rw()
        waw = self.waw(new_read, self.write, new_sched)
        # We don't use new_read for WAR because the self WAR happening within
        # the same iteration is not really a dependence; it's self-implied,
        # because, well, it's the same statement.  It's not useful for I/O
        # sharing optimization either because there's no sharing opportunity
        # in WAR.  Moreover, we're not missing any other WAR dependences
        # either: by using self.read instead of new_read, we remove the split
        # read access and there is no other writes after it that can form a
        # WAR because of the write happening at the same timestamp as the
        # read.
        war = self.war(self.read, self.write, new_sched)
        war = isl_union_map_subtract(war, isl_union_map_copy(waw))
        raw = self.raw(new_read, self.write, new_sched)
        rar = self.rar(new_read, self.write, new_sched)
        rar = isl_union_map_subtract(rar, isl_union_map_copy(raw))
        cur = 0
        for t in [t + 'a' + s for t in ['r', 'w'] for s in ['r', 'w']]:
            umap = locals()[t]
            bmaps = isl_union_map_to_basic_maps(umap)
            for bmap in bmaps:
                dep, access, array = self._build_dependence(bmap)
                dep = Program.merge_self_rw(dep)
                access = Program.merge_self_rw(access)
                d = Dependence(type=t, dep=dep, access=access, array=array,
                               id=cur)
                self.deps.append(d)
                cur += 1
        isl_union_map_free(new_read)
        isl_union_map_free(new_sched)
    
    def print_dependences(self):
        for x in self.deps:
            print (x.id, x.type.enumname, x.arity.enumname,
                  'self' if x.is_self else 'nonself', str(x.dep))
    
    @staticmethod
    def _domain_dims(domain):
        """
        Return a list of pairs (#params, #iter_vars) for each iteration domain.
        Note that #params does not include the constant dimension."""
        sets = isl_union_set_to_sets(isl_union_set_copy(domain))
        dims = [0] * len(sets)
        for s in sets:
            no = domain_id(s)
            dims[no] = (isl_set_n_param(s), isl_set_n_dim(s))
            isl_set_free(s)
        return dims


class DepSet(set):
    def __init__(self, s=[]):
        super(DepSet, self).__init__(s)
    def __hash__(self):
        return hash(tuple(sorted(x.id for x in self)))
    def __repr__(self):
        return '<DepSet> [' + ','.join(str(x) for x in self) + ']'


def _depset_to_pairs(depset):
    res = set()
    for x in depset:
        if x.source < x.target:
            res.add((x.source, x.target))
        elif x.source > x.target:
            res.add((x.target, x.source))
    return res

class Optimizer(object):
    """
    Optimizer that optimizes I/O via loop fusion (or reuse).
    """
    
    def __init__(self, prog):
        """Initialize an optimizer for :py:class:`Program` prog."""
        # prog.print_dependences()
        logger.info(prog.deps)
        self._prog = prog
#        logger.info("%s", [str(x) for x in prog.vars.values()])
        self.deps = DepSet(self._prog.deps)
        """:class:`DepSet` containing all dependences.""" 
        logger.debug('domain dims: %s', self.domain_dims)
        self.max_domain_dim = max((x[1] for x in self.domain_dims))
        logger.debug('max domain dim = %s', self.max_domain_dim)
        map(Dependence.compute_schedules, self.deps, 
            [self.domain_dims]*len(self.deps))
        self.sched_dim = isl_set_get_dim(iter(self.deps).next().sched[0])
        self.real_deps = DepSet(a for a in self.deps if a.is_real)
        self.real_self_deps = DepSet(a for a in self.real_deps if a.is_self)
        self.real_nonself_deps = self.real_deps - self.real_self_deps
        self.input_deps = self.deps - self.real_deps
        self.input_self_deps = DepSet(a for a in self.input_deps if a.is_self)
        self.input_nonself_deps = self.input_deps - self.input_self_deps
    
    def __getattr__(self, attr):
        """For undefined attributes, forward to the :class:`Program` object."""
        return getattr(self._prog, attr)
        
    def dispose(self):
        """Free up resources held."""
        isl_dim_free(self.sched_dim)
        
    def apriori_gen(self, S):
        """
        Generate the candidate set for the next level based on S as
        specified by the apriori algorithm.
        
        Find all subsets T of the total dependence set, such that #T = k+1, 
        and T's all k-subsets are already in S, where S' elements are all
        sets of size k.
        
        C' = {X union Y | X,Y in S and #(X intersect Y)=k-1}
        
        C  = {X in C'| X contains k+1 members of S}
        Return C.
        """
        l = len(S)
        assert(l > 0)
        k = len(iter(S).next())
        if l < k + 1:
            return
        T = sorted(S, key=hash)
        found = set() # found combinations
        for i in range(l):
            for j in range(i):
                x, y = T[i], T[j]
                if len(x & y) == k - 1:
                    c = x | y
                    n = len(filter(c.issuperset, S))
                    if n == k + 1 and c not in found:
                        found.add(c)
                        yield c
    
    def apriori(self):
        """Use the apriori algorithm to enumerate all feasible schedules for
        different reuse configurations.  Return a list of tuples containing
        a feasible reuse configuration and its schedule."""
        n = len(self.deps)
        base = DepSet()
        base_sched = self.is_feasible(base)
        res = [(base, base_sched)] # list of solution (DepSet, schedule) tuples
        # feasible reuse/no-reuse dependences, in dict(level -> set(DepSet)) 
        P = dict() # {DepSet size : set of DepSets with that size}
        # bootstrap: valid, size-zero subsets
        P[0] = set()
        # Valid, size-one subsets. Ignore WAR deps which don't have sharing
        P[1] = set()
        for dep in self.deps:
            if dep.type is Type.WAR:
                continue
            c = DepSet([dep])
            sched = self.is_feasible(c)
            if sched:
                # it may be impractical but helps apriori search, e.g.,
                # is a subset of a larger, practical set
                P[1].add(c)
                self._try_add(res, c, sched)
        # valid size-k subsets (k>1)
        for k in range(2, n + 1):
            # consider all k-subsets that have all their k-1 subsets already
            # found
            P[k] = set()
            for c in self.apriori_gen(P[k - 1]):
                sched = self.is_feasible(c)
                if sched:
                    # it may be impractical but helps apriori search, e.g.,
                    # is a subset of a larger, practical set
                    P[k].add(c)
                    self._try_add(res, c, sched)
            print "finished level", k
            if len(P[k]) == 0:
                break
        logger.info("%d feasible dependence sets", len(res))
        return res
    
    def _try_add(self, result, candidate, schedule):
        if self._is_practical(candidate):
            result.append((candidate, schedule))
        else:
            isl_union_map_free(schedule)

    def _intersect_all(self, S):
        """Intersect all the isl_set's in S."""
        ret = isl_set_universe(isl_dim_copy(self.sched_dim))
        for s in S:
            ret = isl_set_intersect(ret, isl_set_copy(s))
        return ret
    
    def _is_practical(self, P):
        """Check if the given reuse set can never be satisfied.

        Currently the check includes:
        1. If P includes a W->W dep but there is some other W->R dep that is
        not in P, then P is illegal because W->W would remove the source write
        which would make the read in W->R impossible to be a real read.
        """
        ww = [x for x in P if x.type==Type.WAW]
        wr = [x for x in self.deps-P if x.type==Type.RAW]
        for x in ww:
            for y in wr:
                d1 = isl_basic_map_get_dim(x.source_access)
                d2 = isl_basic_map_get_dim(y.source_access)
                b = isl_dim_equal(d1, d2)
                isl_dim_free(d1)
                isl_dim_free(d2)
                if not b:
                    continue
                z = isl_basic_map_intersect(isl_basic_map_copy(x.source_access),
                                            isl_basic_map_copy(y.source_access))
                b = isl_basic_map_is_empty(z)
                isl_basic_map_free(z)
                if not b:
                    return False
        return True

    def is_feasible(self, P=[]):
        """Is the given reuse configuration feasible?
        
        All dependences in P must have their locality satisfied, while
        those in N must NOT have their locality satisfied."""
        # P can be handled by intersecting schedule space of member dependences
        # but N cannot for the following reason.  Each d in P is a conjunction
        # of L terms, where L is the # loop levels and each term describes
        # the schedule space for that level.  Handling N means (not d1) and
        # (not d2) and ... and (not dn), which is a conjunction of disjunctive
        # terms.  Thus the number of terms after expansion becomes L^n.  For
        # L=4 and n=10, the number goes beyond 1 million.
        #logger.info('%d %d', len(P), len(N))
        # for all but the last dimension
        L_other = self._intersect_all((d.sched[0] for d in P))
        # for the last dimension
        L_last  = self._intersect_all((d.sched[1] for d in P))
        feasible = not (isl_set_is_empty(L_other) or isl_set_is_empty(L_last))
        if feasible:
            feasible = self.find_schedule(P, L_other, L_last)
            if feasible:
                logger.info('solution %s', feasible)
        isl_set_free(L_other)
        isl_set_free(L_last)
        logger.info('%sfeasible => %s', (' ' if feasible else '!'), P)
        return feasible
    
    def _enum_row_dep(self, j, n, k, m):
        """Return a tuple of 0/1 indicating if row j should be linearly
        independent of previous rows or not.
        
        j=current row index, n=total #rows, k=#independent rows before
        row j, m=total # linearly independent rows required."""
        if n-j == m-k:
            return (1,)
        return (0,1) # important that 0 preceeds 1
    
    @staticmethod
    def span(ctx, R, n_dim=0):
        """Compute the span of vectors in R.  If R is empty, n_dim tells
        the vector dimensionality."""
        n = len(R)                      # number of vectors
        m = len(R[0]) if R else n_dim   # vector dimension (R can be [])
        dim = isl_dim_set_alloc(ctx, 0, m+n)
        bset = isl_basic_set_universe(isl_dim_copy(dim))
        v = isl_int()
        isl_int_init(v)
        for i in range(m):
            # add constraint for variable x[i], i=0...(m-1)
            # -x[i] + a1 R[1][i] + ... + an R[n][i] = 0
            c = isl_equality_alloc(isl_dim_copy(dim))
            isl_int_set_si(cast(v, isl_int_ptr), -1)
            isl_constraint_set_coefficient(c, isl_dim_set, i, v)
            for j in range(n):
                isl_int_set_si(cast(v, isl_int_ptr), R[j][i])
                isl_constraint_set_coefficient(c, isl_dim_set, m+j, v)
            bset = isl_basic_set_add_constraint(bset, c)
        bset = isl_basic_set_project_out(bset, isl_dim_set, m, n)
        isl_int_clear(v)
        isl_dim_free(dim)
        return bset
    
    def compute_space(self, R, l, n_dim=0):
        """Given the set of row vectors already found, R, and Boolean l,
        return the space of vectors that are linearly independent of 
        vectors in R if l==1 or linearly dependent if l==0.
        
        The isl_set returned contains the zero-vector (0,...,0) if l==0,
        and does not otherwise."""
        bset = self.span(self.ctx, R, n_dim)
        if l==0:
            # can contain the 0-vector
            return isl_set_from_basic_set(bset)
#            zero = isl_point_zero(isl_basic_set_get_dim(bset))
#            zero_set = isl_set_from_point(zero)
#            return isl_set_subtract(isl_set_from_basic_set(bset), zero_set)
        else:
            # cannot contain the 0-vector
            return isl_set_complement(isl_set_from_basic_set(bset))
    
    def _augment_dims(self, s, cur):
        """Augment the dim of s to that of the full schedule.  s does not
        contain the constant and param dims."""
        ptr = 0
        for i in range(self.n_domain):
            count = sum(self.domain_dims[i]) + 1 # 1 for the constant dim
            if i == cur:
                s = isl_set_insert(s, isl_dim_set, ptr, 
                                   1 + self.domain_dims[i][0])
            else:
                s = isl_set_insert(s, isl_dim_set, ptr, count)
            ptr += count
        return s
    
    def _isl_point_to_schedules(self, point, lengths):
        """Convert an isl_point to a list of schedules, one for each iteration
        domain.  The `point` is a multidimensional vector, consisting of
        schedule coefficients for each domain in order.  `lengths` contains
        the dimensionalities of the domains in the same order.  `point` is
        consumed when the method returns."""
        v = isl_int()
        isl_int_init(v)
        ret = []
        pos = 0
        for i in range(len(lengths)):
            count = lengths[i]
            row = [0] * count
            for j in range(count):
                isl_point_get_coordinate(point, isl_dim_set, pos+j,
                                         cast(v, isl_int_ptr))
                row[j] = isl_int_get_si(v)
            ret.append(row)
            pos += count
        isl_int_clear(v)
        isl_point_free(point)
        return ret
    
    def _cst_from_constraints(self, constraints):
        """Pick the last constant dimension for the statements.  The constant
        for each statement should be different.  Some dependences mandate that
        the constant of its target be greater than that of its source."""
        # Use topological sort
        ordered = topological_sort(range(self.n_domain),
                                   [(d.source, d.target) for d in constraints])
        res = [0] * self.n_domain
        for i in range(self.n_domain):
            res[ordered[i]] = i
        return res
        # input = ''
        # for i in range(self.n_domain):
        #     input += '{} {} '.format(i, i) # self-edge means node exists
        # for dep in constraints:
        #     if dep.sched_cst_gt:
        #         input += '{} {} '.format(dep.source, dep.target)
        # proc = subprocess.Popen(['tsort'], stdin=subprocess.PIPE,
        #                         stdout=subprocess.PIPE)
        # output = proc.communicate(input)[0]
        # ordered = output.split()
        # res = [0] * self.n_domain
        # for i in range(self.n_domain):
        #     res[int(ordered[i])] = i
        # return res
    
    def _schedule_from_coefs(self, RS, cst_dim):
        ret = isl_union_map_empty(isl_union_map_get_dim(self.domain))
        v = isl_int()
        isl_int_init(v)
        assert(len(RS) == self.n_domain)
        for i in range(self.n_domain):
            domain_name = "D"+str(i)  # see Porgram.gen_domain_id
            dim = isl_union_map_get_dim(ret)
            dim = isl_dim_add(dim, isl_dim_in, self.domain_dims[i][1])
            dim = isl_dim_add(dim, isl_dim_out, self.max_domain_dim + 1)
            dim = isl_dim_set_tuple_name(dim, isl_dim_in, domain_name)
            bmap = isl_basic_map_universe(dim)
            assert(len(RS[i]) == self.max_domain_dim) # constant dim separate
            for j in range(self.max_domain_dim): # schedule dim number
                c = isl_equality_alloc(isl_basic_map_get_dim(bmap))
                # out
                isl_int_set_si(cast(v, isl_int_ptr), -1)
                isl_constraint_set_coefficient(c, isl_dim_out, j, v)
                # cst
                isl_int_set_si(cast(v, isl_int_ptr), RS[i][j][0])
                isl_constraint_set_constant(c, v)
                # param
                pos = 1
                for k in range(self.domain_dims[i][0]):
                    isl_int_set_si(cast(v, isl_int_ptr), RS[i][j][pos+k])
                    isl_constraint_set_coefficient(c, isl_dim_param,
                                                   k, v)
                # in
                pos += self.domain_dims[i][0]
                for k in range(self.domain_dims[i][1]):
                    isl_int_set_si(cast(v, isl_int_ptr), RS[i][j][pos+k])
                    isl_constraint_set_coefficient(c, isl_dim_in,
                                                   k, v)
                bmap = isl_basic_map_add_constraint(bmap, c)
            # now for the last constant schedule dimension
            c = isl_equality_alloc(isl_basic_map_get_dim(bmap))
            isl_int_set_si(cast(v, isl_int_ptr), -1)
            isl_constraint_set_coefficient(c, isl_dim_out, self.max_domain_dim,
                                           v)
            isl_int_set_si(cast(v, isl_int_ptr), cst_dim[i])
            isl_constraint_set_constant(c, v)
            bmap = isl_basic_map_add_constraint(bmap, c)
            
            umap = isl_union_map_from_map(isl_map_from_basic_map(bmap))
            ret = isl_union_map_union(ret, umap)
        isl_int_clear(v)
#        logger.info('schedule: %s', ret)
        ret = isl_union_map_intersect_domain(ret,
                                             isl_union_set_copy(self.domain))
#        logger.info('schedule with domain %s', ret)
        return ret
    
    def transitive_closure(self, n, base):
        """Compute the transitive closure on the order relationship between
        domains. `n`: number of domains. `base`: pairs of directly ordered
        domains.
        
        Result is a list of *sorted* pairs."""
        x = [([False] * n) for i in range(n)]
        for s, t in base:
            x[s][t] = True
        for k in range(n):
            for i in range(n):
                for j in range(n):
                    x[i][j] = x[i][j] or (x[i][k] and x[k][j])
        return [(i,j) for i in range(n) for j in range(n) if x[i][j]]

    def find_schedule(self, P, L_other, L_last):
        """Find a schedule that satisfies reuse for dependences in P."""
        # import ipdb; ipdb.set_trace()
        L = self.max_domain_dim
        L_all = [L_other] * (L-1) + [L_last]
        K = [0] * self.n_domain      # number of independent rows so far
        RS = [ [] for i in range(self.n_domain) ]  # row space for each stmt
        R0 = set(self.real_deps) # real deps
        real_pairs = set((d.source, d.target)
                         for d in R0 if d.source != d.target)
        closure = self.transitive_closure(self.n_domain, real_pairs)
        other_pairs = set(self.pairs.keys()) - set(closure)
        for d in range(L):
            X = self._intersect_all((d.weak for d in R0))
            X = isl_set_intersect(X, isl_set_copy(L_all[d]))
            if isl_set_is_empty(X):
                return None
            # dimensionality constraints
            for i in range(self.n_domain):
                for l in self._enum_row_dep(d, L, K[i], self.domain_dims[i][1]):
                    cut = [x[1+self.domain_dims[i][0]:] for x in RS[i]]
                    T = self.compute_space(cut, l, self.domain_dims[i][1])
                    T = self._augment_dims(T, i)
                    Ts = isl_set_to_basic_sets(T)
                    found = False
                    for bset in Ts:
                        if found:
                            isl_basic_set_free(bset)
                            continue
                        XX = isl_set_intersect(isl_set_copy(X),
                                              isl_set_from_basic_set(bset))
                        if not isl_set_is_empty(XX):
                            X, XX = XX, X
                            isl_set_free(XX)
                            K[i] += l
                            found = True
                        else:
                            isl_set_free(XX)
                    if found: break
                else: # not found, fail
                    isl_set_free(X)
                    return None
            # try strongly satisfy
            satisfied = []
            for dep in set(R0):
                XX = isl_set_intersect(isl_set_copy(X), 
                                       isl_set_copy(dep.strong))
                if not isl_set_is_empty(XX):
                    X, XX = XX, X
                    satisfied.append(dep)
                    R0.remove(dep)
                isl_set_free(XX)
            #========================================
            # begin try harder (X gets narrowed down)
            # In the end we are interested in a point in the X space, so it
            # does not hurt if we further restrict X as long as it doesn't
            # become empty.
            for d in satisfied:
                pair = (d.source, d.target)
                key = pair if pair[0] < pair[1] else (pair[1], pair[0])
                if pair[0] == pair[1] or not pair in real_pairs:
                    continue
                Y = isl_set_copy(self.pairs[key][d.source < d.target])
                Y = isl_set_intersect(isl_set_copy(X), Y)
                if not isl_set_is_empty(Y):
                    X, Y = Y, X
                    real_pairs.remove(pair)
                isl_set_free(Y)
            for s, t in set(other_pairs):
                for i in range(2):
                    Y = isl_set_copy(self.pairs[(s,t)][i])
                    Y = isl_set_intersect(isl_set_copy(X), Y)
                    if not isl_set_is_empty(Y):
                        X, Y = Y, X
                        other_pairs.remove((s,t))
                        isl_set_free(Y)
                        break
                    isl_set_free(Y)
            # end try harder
            # ==============
            lengths = [sum(x) + 1 for x in self.domain_dims ]
            scheds = self._isl_point_to_schedules(isl_set_sample_point(X),
                                                  lengths)
            for i in range(self.n_domain):
                RS[i].append(scheds[i])
        # Now collect constraints for the last constant dimension
        # Reuse-enforced, self deps have no constraints because for any
        # constant c chose, c-c=0 always holds.
        cst_dim_constraints = set(x for x in P if not x.is_self)
        if R0:
            # Any remaining real deps without reuse condition must be
            # non-self.  Otherwise it is guaranteed to have been satisfied
            # before the last constant schedule dim.
            assert(all(not x.is_self for x in R0))
            cst_dim_constraints |= R0
        cst_dim = self._cst_from_constraints(cst_dim_constraints)
        return self._schedule_from_coefs(RS, cst_dim)


class DefaultCoster(object):
    """The default costing strategy."""
    
    def __init__(self, vars, ctx):
        if isinstance(vars, VariableDict):
            self.vars = vars
        elif isinstance(vars, dict):
            self.vars = VariableDict(vars)
        else:
            raise ValueError('argument should be dict or VariableDict')
        self.temp_vars = set(k for k,v in self.vars.iteritems()
                             if v['type']=='temp')
        self.map_blocksize = self._build_map_blocksize(ctx)
    
    def __del__(self):
        self.dispose()

    def dispose(self):
        if self.map_blocksize:
            isl_union_pw_qpolynomial_free(self.map_blocksize)

    def _build_map_blocksize(self, ctx):
        """For each array in self.vars build a qpolynomial
        [array]->(block size).
        """
        dim = isl_dim_set_alloc(ctx, 0, 0)
        res = isl_union_pw_qpolynomial_zero(dim)
        for name in self.vars:
            ndim = len(self.vars[name]['size'])
            dim = isl_dim_set_alloc(ctx, 0, ndim)
            dim = isl_dim_set_tuple_name(dim, isl_dim_set, name)
            block_size = self.vars.block_size(name)
            numerator = isl_int()
            denominator = isl_int()
            isl_int_init(numerator)
            isl_int_init(denominator)
            isl_int_set_si(cast(numerator, isl_int_ptr), block_size)
            isl_int_set_si(cast(denominator, isl_int_ptr), 1)
            q = isl_qpolynomial_rat_cst(isl_dim_copy(dim),
                                        numerator, denominator)
            isl_int_clear(numerator)
            isl_int_clear(denominator)
            dom = isl_set_universe(dim)
            q = isl_pw_qpolynomial_alloc(dom, q)
            res = isl_union_pw_qpolynomial_add_pw_qpolynomial(res, q)
        return res
    
    def mem_footprint(self, reused, sched, read, write):
        """
        Compute the maximum memory footprint of the execution given a schedule
        and a set of dependences for which reuse is satisfied.
        :param reused: :class:`DepSet` of reuse-satisfied dependences
        :param sched: isl_union_map, the new schedule
        :param access: isl_union_map, the original array access function
        All params are kept, not consumed.
        """
        res = isl_union_map_union(isl_union_map_copy(read),
                                  isl_union_map_copy(write))
        for dep in reused:
            # find all iterations before the target of dep and after the
            # source of dep.  If RAR and target preceeds source, reverse it.
            dep_reduced = isl_basic_map_copy(dep.dep_reduced)
            if dep.type == Type.RAR and not preceeds(dep_reduced, sched):
                dep_reduced = isl_basic_map_reverse(dep_reduced)
            ran = isl_basic_map_range(isl_basic_map_copy(dep_reduced))
            ran = isl_union_set_from_set(isl_set_from_basic_set(ran))
            sr = isl_union_map_intersect_domain(isl_union_map_copy(sched),
                                                ran)
            B = isl_union_map_lex_lt_union_map(isl_union_map_copy(sched),
                                               sr) #prev->target
            B = isl_union_map_reverse(B) # target -> prev
            dom = isl_basic_map_domain(isl_basic_map_copy(dep_reduced))
            dom = isl_union_set_from_set(isl_set_from_basic_set(dom))
            sd = isl_union_map_intersect_domain(isl_union_map_copy(sched),
                                                dom)
            F = isl_union_map_lex_lt_union_map(sd,
                                               isl_union_map_copy(sched))
            tmp = isl_map_from_basic_map(isl_basic_map_copy(dep_reduced))
            tmp = isl_union_map_from_map(tmp)
            F = isl_union_map_apply_domain(F, tmp) # target -> after
            between = isl_union_map_intersect(F, B) #target -> in-between
            # next link it with the array elements accessed by the target
            tmp = isl_map_from_basic_map(isl_basic_map_copy(dep.target_access))
            tmp = isl_union_map_from_map(tmp) # [target]->[array]
            btw_access = isl_union_map_apply_domain(tmp, between)
            res = isl_union_map_union(res, btw_access)
            isl_basic_map_free(dep_reduced)
        # now check each statement (domain) in res for the array
        # elements that need to be in memory while it is executing
        return self.mem_upperbound(res)


    def mem_upperbound(self, umap):
        """Take a [iteration]->[array] and return the upperbound of memory size
        across all iterations.  `umap` is consumed."""
        iter_size = self.iteration_to_size(umap)
        tight = c_int()
        fold = isl_union_pw_qpolynomial_bound(iter_size, isl_fold_max,
                                              byref(tight))
        fold = isl_union_pw_qpolynomial_fold_coalesce(fold)
        return (fold)


    def iteration_to_size(self, umap):
        """Take the given isl_union_map `umap` and build a map
        [iteration]->(memory size).  `umap` is consumed.

        umap is a union map of [iteration]->[array].  We first construct a union
        piecewise qpolynomial [array]->p which represents the size of each array
        element (or block, depending on the context). p should be the same for
        the same array, but may be different for different arrays.  For array
        elements, p should be a constant; for array blocks, p should be a
        monomial with block dimension sizes as factors.  Applying [array]->p to
        the original umap gives [iteration]->p, total memory required by each
        iteration."""
        array_size = isl_union_pw_qpolynomial_copy(self.map_blocksize)
        return isl_union_map_apply_union_pw_qpolynomial(umap, array_size)


    def array_to_size(self, umap):
        """For each array in the range of `umap` build a mapping
        [array]->(block size).  `umap` is consumed.
        """
        # TODO: build this once during Coster initialization
        arrays = set() # one term for each array
        res = isl_union_pw_qpolynomial_zero(isl_union_map_get_dim(umap))
        maps = isl_union_map_to_maps(umap)
        for m in maps:
            dim = isl_map_get_dim(m)
            dim = isl_dim_range(dim)
            name = isl_dim_get_tuple_name(dim, isl_dim_set).data
            if name not in arrays:
                arrays.add(name)
                block_size = self.vars.block_size(name)
                numerator = isl_int()
                denominator = isl_int()
                isl_int_init(numerator)
                isl_int_init(denominator)
                isl_int_set_si(cast(numerator, isl_int_ptr), block_size)
                isl_int_set_si(cast(denominator, isl_int_ptr), 1)
                q = isl_qpolynomial_rat_cst(isl_dim_copy(dim),
                                            numerator, denominator)
                isl_int_clear(numerator)
                isl_int_clear(denominator)
                dom = isl_set_universe(isl_dim_copy(dim))
                q = isl_pw_qpolynomial_alloc(dom, q)
                res = isl_union_pw_qpolynomial_add_pw_qpolynomial(res, q)
            isl_dim_free(dim)
            isl_map_free(m)
        return res


    def array_to_size_rw(self, r_umap, w_umap):
        """Same as `array_to_size` but handle read ans writes separately."""
        return (self.array_to_size(r_umap), self.array_to_size(w_umap))

    def iteration_to_size_rw(self, r_umap, w_umap):
        """Same as `iteration_to_size` but handle reads and writes separately.
        """
        r_array_size = self.array_to_size(isl_union_map_copy(r_umap))
        r = isl_union_map_apply_union_pw_qpolynomial(r_umap, r_array_size)
        w_array_size = self.array_to_size(isl_union_map_copy(w_umap))
        w = isl_union_map_apply_union_pw_qpolynomial(w_umap, w_array_size)
        return isl_union_pw_qpolynomial_add(r, w)

    def io_cost(self, reused, sched, read, write):
        """Compute the I/O cost of the execution given the complete access
        relation of the program.

        The basic idea is to first compute the data accessed by each iteration.
        Such data need to be read/written unless it induces a satisfied reuse.
        The I/O cost is calculated by a subtract operation.
        """
        # Compute the I/O size for reads and writes separately, because a
        # single statement may read *and* write the same array element, causing
        # two I/Os.
        r_total = self.iteration_to_size(isl_union_map_copy(read))
        w_total = self.iteration_to_size(isl_union_map_copy(write))
        r_saving = isl_union_map_empty(isl_union_map_get_dim(read))
        w_saving = isl_union_map_empty(isl_union_map_get_dim(read))
        for dep in reused:
            assert(dep.type is not Type.WAR)
            if dep.type is Type.WAW:
                tmp = isl_basic_map_copy(dep.source_access_reduced)
                tmp = isl_map_from_basic_map(tmp)
                tmp = isl_union_map_from_map(tmp)
                w_saving = isl_union_map_union(w_saving, tmp)
            else: # RAW and RAR
                tmp = isl_basic_map_copy(dep.target_access_reduced)
                tmp = isl_map_from_basic_map(tmp)
                tmp = isl_union_map_from_map(tmp)
                r_saving = isl_union_map_union(r_saving, tmp)
        reused_size = self.iteration_to_size(r_saving)
        r_total = isl_union_pw_qpolynomial_sub(r_total, reused_size)
        reused_size = self.iteration_to_size(w_saving)
        reused_size = isl_union_pw_qpolynomial_add(reused_size,
                            self._temp_write_vol(reused, sched, read, write))
        w_total = isl_union_pw_qpolynomial_sub(w_total, reused_size)
        return (isl_union_pw_qpolynomial_sum(r_total),
                isl_union_pw_qpolynomial_sum(w_total))

    def _temp_write_vol(self, reused, sched, read, write):
        """Return the data volume of writes to temp variables that are never
        read again.  These I/Os can be optimized away."""
        # For each temp variable x, find the last writes to x that satisfy the
        # following: either there is no read of x after the write, or if there
        # is a read, it must be the target of a satisfied reuse relation
        real_reads = isl_union_map_copy(read) # reads not optimized away
        # res = isl_union_set_empty(isl_union_map_get_dim(read))
        res = isl_union_map_empty(isl_union_map_get_dim(read))
        for dep in reused:
            if dep.type is not Type.RAW and dep.type is not Type.RAR:
                continue
            tmp = isl_basic_map_copy(dep.target_access_reduced)
            tmp = isl_union_map_from_map(isl_map_from_basic_map(tmp))
            real_reads = isl_union_map_subtract(real_reads, tmp)
        # a2t_[r|w] is [array]->[time]
        a2t_w = isl_union_map_apply_domain(isl_union_map_copy(sched),
                                           isl_union_map_copy(write))
        a2t_r = isl_union_map_apply_domain(isl_union_map_copy(sched),
                                           real_reads)
        # last_writes = isl_union_map_empty(isl_union_map_get_dim(sched))
        # last_reads  = isl_union_map_empty(isl_union_map_get_dim(sched))
        last_writes = dict() # array_name -> [ array -> time]
        last_reads = dict()  # array_name -> [ array -> time]
        domains = dict()     # array_name -> [ domain -> array]
        for m in isl_union_map_to_maps(a2t_w):
            name = isl_map_get_tuple_name(m, isl_dim_in).data
            name = name.split('_')[0]
            if name in self.temp_vars:
                max = isl_map_lexmax(m)
                last_writes[name] = max # array -> time
                tmp = isl_union_map_from_map(
                    isl_map_reverse(isl_map_copy(max)))
                domains[name] = isl_union_map_apply_range(
                    isl_union_map_copy(sched), tmp)
            else:
                isl_map_free(m)
        for m in isl_union_map_to_maps(isl_union_map_coalesce(a2t_r)):
            name = isl_map_get_tuple_name(m, isl_dim_in).data
            name = name.split('_')[0]
            if name in self.temp_vars:
                max = isl_map_lexmax(m)
                last_reads[name] = max
            else:
                isl_map_free(m)
        for k,v in last_writes.iteritems():
            if k in last_reads:
                tmp = isl_map_copy(v)
                follow = isl_map_lex_lt_map(tmp, last_reads[k])
                useless = isl_set_subtract(isl_map_domain(v),
                                           isl_map_domain(follow))
                useless = isl_union_set_from_set(useless)
                tmp = isl_union_map_intersect_range(domains[k], useless)
                res = isl_union_map_union(res, tmp)
                del last_reads[k]
            else:
                res = isl_union_map_union(res, domains[k])
                isl_map_free(v)
        for v in last_reads.itervalues():
            isl_map_free(v)
        return isl_union_map_apply_union_pw_qpolynomial(res,
                    isl_union_pw_qpolynomial_copy(self.map_blocksize))


class FixedBlockOptimizer(Optimizer):

    def __init__(self, prog):
        super(FixedBlockOptimizer, self).__init__(prog)
        self._prog = prog
        # cost in terms of time (sec)
        # read speed 94MB/s, write speed 60MB/s
        self.READ_COST = 8.0/2**20/96
        self.WRITE_COST = 8.0/2**20/60 

    def param_to_point(self, obj):
        """Wrap the global params in an isl_point object."""
        dim = isl_func(obj, 'get_dim')(obj)
        n_dim = isl_dim_size(dim, isl_dim_param)
        point = isl_point_zero(isl_dim_copy(dim))
        v = isl_int()
        isl_int_init(v)
        for i in range(n_dim):
            param_name = isl_dim_get_name(dim, isl_dim_param, i)
            param_val  = self.vars.array_size_in_blocks(param_name)
            isl_int_set_si(cast(v, isl_int_ptr), param_val)
            point = isl_point_set_coordinate(point, isl_dim_param, i, v)
        isl_dim_free(dim)
        isl_int_clear(v)
        return point
    
    def _eval(self, obj):
        point = self.param_to_point(obj)
        v = isl_int()
        v1 = isl_int()
        isl_int_init(v)
        isl_int_init(v1)
        qpoly = isl_func(obj, 'eval')(obj, point)
        is_cst = isl_qpolynomial_is_cst(qpoly, cast(v, isl_int_ptr),
                                        cast(v1, isl_int_ptr))
        isl_free(qpoly)
        assert(is_cst and isl_int_cmp_si(v1, 1)==0)
        res = isl_int_get_si(v)
        isl_int_clear(v)
        isl_int_clear(v1)
        return res
        
    def _is_actually_empty(self, dep):
        dep = isl_basic_map_copy(dep.dep)
        dim = isl_basic_map_get_dim(dep)
        n_dim = isl_dim_size(dim, isl_dim_param)
        for i in range(n_dim):
            param_name = isl_dim_get_name(dim, isl_dim_param, i)
            param_val  = self.vars.array_size_in_blocks(param_name)
            dep = isl_basic_map_fix_si(dep, isl_dim_param, i, param_val)
        isl_dim_free(dim)
        res = isl_basic_map_is_empty(dep)
        isl_basic_map_free(dep)
        return res

    def select_plan(self, total_mem, file=sys.stdout):
        coster = DefaultCoster(self.vars, self.ctx)
        candidates = self.apriori()
        ranked = []
        for depset,sched in candidates:
            # substitute symbolic params with real values and check if any
            # dep is actually empty
            if any(self._is_actually_empty(x) for x in depset):
                continue
            mem_s = coster.mem_footprint(depset, sched, self.read, self.write)
            mem = self._eval(mem_s) * 8.0 / 2**20
            if mem > total_mem:
                pass
            else:
                io = coster.io_cost(depset, sched, self.read, self.write)
                (r, w) = map(self._eval, io)
                c  = r * self.READ_COST + w * self.WRITE_COST
                ranked.append((depset, sched, mem, c, r, w))
        # sort by IO cost
        ranked = sorted(ranked, key=lambda x: x[3], reverse=True)
        for i,x in enumerate(ranked):
            print x[0]
            print i, 'mem=', x[2], 'io=', x[3]
            s = islrf_codegen(isl_union_map_copy(x[1]),
                              self.param_to_point(x[1]))
            comment_s = s.data
            print comment_s
            islrf_free(s)
            file.write('# {}\n'.format(x[0]))
            file.write(re.sub('^', '# ', comment_s, flags=re.MULTILINE))
            file.write('\n{} {} {} {} {}\n'.format(i, x[2], x[3], x[4], x[5]))
        for depset, sched in candidates:
            isl_union_map_free(sched)


if __name__ == "__main__":
    import unittest
    suite = unittest.defaultTestLoader.discover(os.path.dirname(__file__))
    unittest.TextTestRunner(verbosity=2).run(suite)
