from riot.isl import *
from riot.islrf import islrf_reduce_arity, islrf_codegen
from riot.islrf import islrf_union_map_compute_flow
from riot.barvinok import isl_union_map_apply_union_pw_qpolynomial
from riot.barvinok import isl_union_set_apply_union_pw_qpolynomial
from riot.barvinok import isl_union_pw_qpolynomial_sum


_this_module = sys.modules[__name__]


isl_int_ptr = mpz_ptr


# isl_basic_map_insert is not declared in the isl headers so not detected by
# ctypesgen and thus not present in isl.py
libisl = CDLL(ctypes.util.find_library('isl'))
isl_basic_map_insert = libisl.isl_basic_map_insert
isl_basic_map_insert.argtypes = [POINTER(isl_basic_map),
                                 enum_isl_dim_type,
                                 c_uint, c_uint]
isl_basic_map_insert.restype = POINTER(isl_basic_map)
islrf_reduce_arity.restype = POINTER(isl_basic_map)
isl_union_map_apply_union_pw_qpolynomial.restype = POINTER(isl_union_pw_qpolynomial)
isl_union_set_apply_union_pw_qpolynomial.restype = POINTER(isl_union_pw_qpolynomial)
isl_union_pw_qpolynomial_sum.restype = POINTER(isl_union_pw_qpolynomial)


def isl_basic_set_insert(bset, dim_type, pos, n):
    bmap = isl_basic_map_insert(bset, dim_type, pos, n)
    return cast(bmap, POINTER(isl_basic_set))


_libpath = "."


def add_lib_path(path):
    """Add a path where riotfusion libraries/scripts can be found."""
    global _libpath
    _libpath += os.pathsep + path


def find_file_in_lib(filename):
    """Given a file, find it in the given lib paths"""
    global _libpath
    paths = _libpath.split(os.pathsep)
    for path in paths:
        fullpath = os.path.join(path, filename)
        if os.path.exists(fullpath):
            return os.path.abspath(fullpath)
    else:
        raise IOError('file %s not found in riot operator library path' % (filename))


def gen_eq_constraint(dim, dim_type_1, dim_pos_1, dim_type_2, dim_pos_2):
    """Return a equality constraint that says the variable of type dim_type_1
    at index dim_pos_1 is equal to the variable of type dim_type_2 at index
    dim_pos_2.  The dim object is consumed, so caller should make a copy."""
    constraint = isl_equality_alloc(dim)
    isl_constraint_set_coefficient_si(constraint, dim_type_1, dim_pos_1, 1)
    if dim_type_2 == isl_dim_cst:
        isl_constraint_set_constant_si(constraint, -dim_pos_2)
    else:
        isl_constraint_set_coefficient_si(constraint, dim_type_2, dim_pos_2,-1)
    return constraint


def isl_type_name(obj):
    """Return the name of the type of obj.  Must be isl_xxx."""
    s = str(type(obj))
    return s[s.find('struct_isl_') + len('struct_isl_'):s.rfind("'")]


def isl_func(obj, s):
    """Return the function named `s` on obj's type."""
    return getattr(_this_module, 'isl_'+isl_type_name(obj)+'_'+s)


def isl_to_str(self):
    """Return an isl_xxx object's string representation.
    
    If the input is of type isl_xxx, there must be an existing function
    isl_printer_print_xxx in the isl library."""
#    s = str(self)
#    name = s[s.find('struct_isl_') + len('struct_isl_'):s.rfind(" object")]
    name = isl_type_name(self)
    ctx_func = getattr(_this_module, 'isl_'+name+'_get_ctx')
    print_func = getattr(_this_module, 'isl_printer_print_' + name)
    ctx = ctx_func(self)
    printer = isl_printer_to_str(ctx)
    printer = print_func(printer, self)
    s = isl_printer_get_str(printer)[:]
    isl_printer_free(printer)
    return s.data


def isl_free(obj):
    """Return an isl_xxx object's free function.

    If the input is of type isl_xxx, there must be an existing function
    isl_xxx_free in the isl library."""
    isl_func(obj, 'free')(obj)


def is_isl_obj(obj):
    """Check if the given object is of type isl_xxx."""
    s = str(type(obj))
    return s.find('struct_isl_') != -1


def isl_is_equal(obj, other):
    """Test if the given obj is equal to the object represented by other (str).
    """
    eq_func = isl_func(obj, 'is_equal')
    ctx = isl_func(obj, 'get_ctx')(obj)
    read_func = isl_func(obj, 'read_from_str')
    if len(read_func.argtypes) == 3:
        other_obj = read_func(ctx, other, -1)
    else:
        other_obj = read_func(ctx, other)
    res = eq_func(obj, other_obj) != 0
    isl_free(other_obj)
    return res

POINTER(struct_isl_basic_set).__repr__ = isl_to_str
POINTER(struct_isl_set).__repr__ = isl_to_str
POINTER(struct_isl_union_set).__repr__ = isl_to_str
POINTER(struct_isl_basic_map).__repr__ = isl_to_str
POINTER(struct_isl_map).__repr__ = isl_to_str
POINTER(struct_isl_union_map).__repr__ = isl_to_str
POINTER(struct_isl_dim).__repr__ = isl_to_str
POINTER(struct_isl_qpolynomial).__repr__ = isl_to_str
POINTER(struct_isl_pw_qpolynomial).__repr__ = isl_to_str
POINTER(struct_isl_union_pw_qpolynomial).__repr__ = isl_to_str


def isl_set_to_basic_sets(m):
    """Flatten the given set into a list of basic sets. m is consumed."""
    res = []
    FUNC_set = isl_set_foreach_basic_set.argtypes[1]
    def for_basic_set(bset, data):
        # if there is user data, package it in a tuple
        res.append(bset)
        return 0
    isl_set_foreach_basic_set(m, FUNC_set(for_basic_set), None)
    isl_set_free(m)
    return res


def isl_union_set_to_basic_sets(uset):
    """Flatten the given union set into a list of basic sets. uset is consumed.
    """
    res = []
    FUNC_Uset = isl_union_set_foreach_set.argtypes[1]
    def for_set(m, data):
        res.extend(isl_set_to_basic_sets(m))
        return 0
    isl_union_set_foreach_set(uset, FUNC_Uset(for_set), None)
    isl_union_set_free(uset)
    return res


def isl_union_set_to_sets(uset):
    """Flatten the given union set into a list of sets. uset is consumed.
    """
    res = []
    FUNC_Uset = isl_union_set_foreach_set.argtypes[1]
    def for_set(m, data):
        res.append(m)
        return 0
    isl_union_set_foreach_set(uset, FUNC_Uset(for_set), None)
    isl_union_set_free(uset)
    return res


def isl_map_to_basic_maps(m):
    """Flatten the given map into a list of basic maps. m is consumed."""
    res = []
    FUNC_MAP = isl_map_foreach_basic_map.argtypes[1]
    def for_basic_map(bmap, data):
        # if there is user data, package it in a tuple
        res.append(bmap)
        return 0
    isl_map_foreach_basic_map(m, FUNC_MAP(for_basic_map), None)
    isl_map_free(m)
    return res


def isl_union_map_to_basic_maps(umap):
    """Flatten the given union map into a list of basic maps. umap is consumed.
    """
    res = []
    FUNC_UMAP = isl_union_map_foreach_map.argtypes[1]
    def for_map(m, data):
        res.extend(isl_map_to_basic_maps(m))
        return 0
    isl_union_map_foreach_map(umap, FUNC_UMAP(for_map), None)
    isl_union_map_free(umap)
    return res


def isl_union_map_to_maps(umap):
    """Flatten the given union map into a list of maps. umap is consumed.
    """
    res = []
    FUNC_UMAP = isl_union_map_foreach_map.argtypes[1]
    def for_map(m, data):
        res.append(m)
        return 0
    isl_union_map_foreach_map(umap, FUNC_UMAP(for_map), None)
    isl_union_map_free(umap)
    return res


def isl_basic_map_identity_from_basic_set(bset):
    """Construct an identity basic map from the given basic set.  bset is
    consumed."""
    dim = isl_dim_map_from_set(isl_basic_set_get_dim(bset))
    bmap = isl_basic_map_identity(dim)
    return isl_basic_map_intersect_range(bmap, bset)


def isl_basic_map_is_identity(bmap):
    """Check if the given basic map is an identity map, which maps x to itself.
    bmap is not consumed."""
    res = False
    dom = isl_basic_map_domain(isl_basic_map_copy(bmap))
    id_bmap = isl_basic_map_identity_from_basic_set(dom)
    if isl_basic_map_is_equal(bmap, id_bmap):
        res = True
    isl_basic_map_free(id_bmap)
    return res


def domain_id(s):
    """Return the id (int) of the given isl_set."""
    name = isl_set_get_tuple_name(s, isl_dim_set).data
    return int(name[1:])


def preceeds(bmap, sched):
    """Return True if source of `bmap` preceeds target under `sched`."""
    bmap = isl_basic_map_copy(bmap)
    n = isl_basic_map_n_in(bmap)
    in_name = isl_basic_map_get_tuple_name(bmap, isl_dim_in).data
    out_name = isl_basic_map_get_tuple_name(bmap, isl_dim_out).data
    for x in isl_union_map_to_basic_maps(isl_union_map_copy(sched)):
        name = isl_basic_map_get_tuple_name(x, isl_dim_in)
        if name == in_name:
            bmap = isl_basic_map_apply_domain(bmap, isl_basic_map_copy(x))
        if name == out_name:
            bmap = isl_basic_map_apply_range(bmap, isl_basic_map_copy(x))
        isl_basic_map_free(x)
    bset = isl_basic_map_wrap(bmap)
    point = isl_basic_set_sample_point(bset)
    a = isl_int()
    isl_int_init(a)
    ret = True
    for i in range(n):
        isl_point_get_coordinate(point, isl_dim_set, i, cast(a, isl_int_ptr))
        v1 = isl_int_get_si(a)
        # there's a constant dim, so counterpart is n+1 dims away
        isl_point_get_coordinate(point, isl_dim_set, i+n+1,
                                 cast(a, isl_int_ptr))
        v2 = isl_int_get_si(a)
        if v1 < v2:
            ret = True
            break
        elif v1 > v2:
            ret = False
            break
        else:
            pass
    isl_int_clear(a)
    isl_point_free(point)
    return ret

class EqClass(object):
    """
    Equivalent class relationship as defined by equivalent instances.

    For example, given a=b=c, c=d, and e=f, we can deduce two equivalent
    classes: (a,b,c,d) and (e,f).  Each call of the :py:func:`add` function adds 
    some equivalent objects.  After acquiring enough knowledge about object
    equivalence, :py:func:`partition` can be called to partition a set of 
    objects into equivalent classes.
    """
    def __init__(self):
        self.membership = {} # dict from obj to its class (index)
        self.classes = [] # seq of sets
        self.representatives = [] # representative for each class

    def add(self, objs):
        """add objects which are all equivalent"""
        for obj in objs:
            if obj in self.membership:
                index = self.membership[obj]
                c = self.classes[index]
                break
        else:
            c = set()
            index = len(self.classes)
            self.classes.append(c)
            self.representatives.append(objs[0])
        map(c.add, objs)
        for obj in objs:
            self.membership[obj] = index

    def partition(self, objs):
        """partition the given objects according to this equivalence class"""
        groups = [None] * len(self.classes)
        for obj in objs:
            c = self.membership[obj]
            if groups[c] is None:
                groups[c] = set()
            groups[c].add(obj)
        return filter(lambda x: x[0] is not None, zip(groups, self.representatives))
    def __str__(self):
        return str(self.classes)
