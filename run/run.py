#!/usr/bin/env python
import os, sys
import getopt
from riot.fusion import *
from riot.parser import *

def main():
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], "m:")
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)
    memcap = sys.maxint
    if len(args) != 1:
        assert False, "exactly one input filename needed"
    for o,a in opts:
        if o == '-m':
            memcap = int(a)
        else:
            assert False, "unknown option"
    curdir = os.path.dirname(__file__)
    add_lib_path(os.path.dirname(args[0]))
    prog = YamlProgramParser(args[0]).parse()
    plans_file = os.path.splitext(args[0])[0] + '.plans'
    opt = FixedBlockOptimizer(prog)
    opt.select_plan(memcap, open(plans_file,'w'))
    print 'memory cap = {}'.format(memcap)
    opt.dispose()
    prog.dispose()

if __name__ == "__main__":
    main()
