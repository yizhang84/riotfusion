from unittest import TestCase
from riot.fusion import *
# from riot.costing import *
from logging import info, warn

class TestCosting(TestCase):
    
    def setUp(self):
        self.ctx = isl_ctx_alloc()

    def tearDown(self):
        isl_ctx_free(self.ctx)

    def test_simple(self):
        coster = DefaultCoster({'c': {'size':[1]}})
        def _test_mem_upperbound(input, output):
            umap = isl_union_map_read_from_str(self.ctx, input)
            ub = coster.mem_upperbound(umap)
            self.assertEqual(ub, output)
        _test_mem_upperbound(
            '[c_1]->{D1[i]->c[i]:0<=i<=c_1; D2[i]->d[i]:0<=i<=c_1}', 1)
            # '[n] -> { max(1) : n >= 0 }')
        _test_mem_upperbound(
            '[c_1]->{D1[i]->c[i]:0<=i<=c_1; D1[i]->d[i]:0<=i<=c_1;' +
            'D2[i]->d[i]:0<=i<=c_1}',
            2)
            # '[n] -> { max(2) : n >= 0 }')

    def _build_dependence(self, type, dep, access, array, id):
        dep = isl_basic_map_read_from_str(self.ctx, dep, -1)
        access = isl_basic_map_read_from_str(self.ctx, access, -1)
        return Dependence(type, dep, access, array, id)

    def test_mem_footprint_and_io_cost(self):
        """Test the memory footprint and I/O cost of the following program:
        for (i=0; i<=n; ++i)
          for (j=0; j<=n; ++j)
            a[i] += b[j]; // D1
            c[i] += d[j]; // D2
            e[j] += b[j]; // D3
        """
        coster = DefaultCoster({'a': {'size':[1]}})
        d0 = self._build_dependence(Type.WAW,
                                    '[a_1]->{D1[i,j]->D1[i,j+1]:0<=i<=a_1 and 0<=j<a_1}',
                                    '[a_1]->{D1[i,j]->a[i]:0<=i<=a_1 and 1<=j<=a_1}',
                                    'a', 0)
        d1 = self._build_dependence(Type.WAW,
                                    '[a_1]->{D2[i,j]->D2[i,j+1]:0<=i<=a_1 and 0<=j<a_1}',
                                    '[a_1]->{D2[i,j]->c[i]:0<=i<=a_1 and 1<=j<=a_1}',
                                    'c', 1)
        d2 = self._build_dependence(Type.RAR,
                                    '[a_1]->{D1[i,j]->D3[i,j]:0<=i<=a_1 and 0<=j<=a_1}',
                                    '[a_1]->{D3[i,j]->b[j]:0<=i,j<=a_1}',
                                    'b', 2)
        reused = DepSet([d0, d1, d2])
        domain = '[a_1]->{D1[i,j]: 0<=i,j<=a_1; D2[i,j]: 0<=i,j<=a_1; D3[i,j]: 0<=i,j<=a_1}'
        domain = isl_union_set_read_from_str(self.ctx, domain)
        sched = '{D1[i,j]->[i,j,0]; D2[i,j]->[i,j,1]; D3[i,j]->[i,j,2]}'
        sched = isl_union_map_read_from_str(self.ctx, sched)
        sched = isl_union_map_intersect_domain(sched,
                                               isl_union_set_copy(domain))
        access = '{D1[i,j]->a[i]; D1[i,j]->b[j]; D2[i,j]->c[i]; D2[i,j]->d[j]; D3[i,j]->e[j]; D3[i,j]->b[j]}'
        access = isl_union_map_read_from_str(self.ctx, access)
        access = isl_union_map_intersect_domain(access,
                                               isl_union_set_copy(domain))
        res = coster.mem_footprint(reused, sched, access)
        # expected result: if n==0, b[j] needs to be in memory while D2 is
        # executing; if n>=1, a[i] also needs to be in memory.
        # ans = '[n] -> { max(4) : n >= 1; max(3) : n = 0 }'
        self.assertEqual(res, 4)
        # isl_union_pw_qpolynomial_fold_free(res)
        io = coster.io_cost(reused, access)
        # ans = '[n] -> { (5 + 8 * n + 3 * n^2) : n >= 1; 5 : n = 0 }'
        self.assertEqual(io, 5+8*1+3*(1**2))
        # isl_union_pw_qpolynomial_free(io)
        for dep in reused:
            dep.dispose()
        isl_union_map_free(sched)
        isl_union_map_free(access)
        isl_union_set_free(domain)
