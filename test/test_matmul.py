import os
import logging
import unittest
from riot.fusion import *
from riot.parser import *

class TestMatMul(unittest.TestCase):
    
    def setUp(self):
        # loggin.basicConfig(level=INFO)
        curdir = os.path.dirname(__file__)
        add_lib_path(os.path.join(curdir, ".."))
        self.prog = YamlProgramParser(os.path.join(curdir,
                                                   "mm-mm.yaml")).parse()
        self.opt = FixedBlockOptimizer(self.prog)

    def test_apriori(self):
        self.opt.select_plan(9999)

    def tearDown(self):
        self.opt.dispose()
        self.prog.dispose()
