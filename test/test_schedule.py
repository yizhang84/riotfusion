from riot.fusion import *
import os
from logging import *
import unittest

class TestSchedule(unittest.TestCase):
    def setUp(self):
        basicConfig(level=INFO)
        curdir = os.path.dirname(__file__)
        add_lib_path(os.path.join(curdir, "../.."))
        self.ctx = isl_ctx_alloc()
    def test_other_2(self):
        dep = isl_basic_map_read_from_str(self.ctx, '[n]->{D1[i,j]->D2[ii,jj]:0<=i,ii,jj<=n and 0<=j<=n-i and i=ii and i+j=jj}', -1)
        d = Dependence(dep=dep, type='RAW')
        c = d.get_schedule(relation=Relation.EQ, const=0)
        c1 = isl_basic_set_read_from_str(self.ctx, '{[c,n,i,j,c,n,i-j,j]}', -1)
        self.assertTrue(isl_basic_set_is_equal(c, c1))
        isl_basic_set_free(c)
        isl_basic_set_free(c1)
        d.dispose()
    def test_self(self):
        dep = isl_basic_map_read_from_str(self.ctx, '[n]->{D1[i]->D1[i+1]:0<=i<n}', -1)
        d = Dependence(dep=dep, type='RAW')
        c = d.get_schedule(relation=Relation.GE)
        c1 = isl_basic_set_read_from_str(self.ctx, '{[i,j,k]: k>=0}', -1)
        self.assertTrue(isl_basic_set_is_equal(c, c1))
        isl_basic_set_free(c)
        isl_basic_set_free(c1)
        d.dispose()
    def test_one_one(self):
        dep = isl_basic_map_read_from_str(self.ctx, '[n]->{D1[i]->D2[i+1]:0<=i<n}', -1)
        d = Dependence(dep=dep, type='RAW')
        c = d.get_schedule(relation=Relation.EQ, const=0)
        c1 = isl_basic_set_read_from_str(self.ctx, '{[c,n,i,c-i,n,i]}', -1)
        self.assertTrue(isl_basic_set_is_equal(c, c1))
        isl_basic_set_free(c)
        isl_basic_set_free(c1)
        d.dispose()
    def test_one_many_eq(self):
        dep = isl_basic_map_read_from_str(self.ctx, '[n]->{D1[i]->D2[j]:0<=i<n and i<j<n}', -1)
        d = Dependence(dep=dep, type='RAW')
        c = d.get_schedule(relation=Relation.EQ, const=0)
        c1 = isl_basic_set_read_from_str(self.ctx, '{[c,n,i,c-i,n,i]}', -1)
        self.assertTrue(isl_basic_set_is_equal(c, c1))
        isl_basic_set_free(c)
        isl_basic_set_free(c1)
        d.dispose()
    # def test_one_many_ge(self):
    #     dep = isl_basic_map_read_from_str(self.ctx, '[n]->{D1[i]->D2[j]:0<=i<n and i<j<n}', -1)
    #     d = Dependence(dep=dep, type='RAW')
    #     print d.dep
    #     c = d.get_schedule(relation=Relation.GE, const=1)
    #     print c
    #     c1 = isl_basic_set_read_from_str(self.ctx, '{[c,n,i,c-i,n,i]}', -1)
    #     self.assertTrue(isl_basic_set_is_equal(c, c1))
    #     isl_basic_set_free(c)
    #     isl_basic_set_free(c1)
    #     d.dispose()
    def test_many_one_eq(self):
        dep = isl_basic_map_read_from_str(self.ctx, '[n]->{D1[i]->D2[j]:j=0 and 0<i<n}', -1)
        d = Dependence(dep=dep, type='RAW')
        c = d.get_schedule(relation=Relation.EQ, const=1)
        c1 = isl_basic_set_read_from_str(self.ctx, '{[c,n,i,c+i+1,n,ii]}', -1)
        self.assertTrue(isl_basic_set_is_equal(c, c1))
        isl_basic_set_free(c)
        isl_basic_set_free(c1)
        d.dispose()
    def test_many_many_eq(self):
        dep = isl_basic_map_read_from_str(self.ctx, '[n]->{D1[i]->D2[j]:i<=j<i+2 and 0<=i<n}', -1)
        d = Dependence(dep=dep, type='RAW')
        c = d.get_schedule(relation=Relation.EQ, const=1)
        c1 = isl_basic_set_read_from_str(self.ctx, '{[c,n,i,c+1,n,i]}', -1)
        self.assertTrue(isl_basic_set_is_equal(c, c1))
        isl_basic_set_free(c)
        isl_basic_set_free(c1)
        d.dispose()
    def tearDown(self):
        isl_ctx_free(self.ctx)
