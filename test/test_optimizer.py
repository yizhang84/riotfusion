from riot.fusion import *
from riot.parser import *
import os
from logging import *
import unittest

class TestOptimizer(unittest.TestCase):
    
    def setUp(self):
        basicConfig(level=INFO)
    
    def test_compute_space(self):
        R=[(1,2,0), (1,0,1)]
        ctx = isl_ctx_alloc()
        bset = Optimizer.span(ctx, R)
        ans = isl_basic_set_read_from_str(ctx, "{ [i, j, k] : 2k = 2i - j }",
                                          -1)
        self.assertTrue(isl_basic_set_is_equal(bset, ans))
        isl_basic_set_free(bset)
        isl_basic_set_free(ans)
        isl_ctx_free(ctx)
    
