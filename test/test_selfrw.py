import os
import logging
import unittest
from riot.fusion import *
from riot.parser import *

class TestSelfRW(unittest.TestCase):
    
    def setUp(self):
        logging.basicConfig(level=logging.INFO)
        self.dir = os.path.dirname(__file__)
        add_lib_path(os.path.join(self.dir, "."))
        add_lib_path(os.path.join(self.dir, ".."))
    def test(self):
        prog = YamlProgramParser(os.path.join(self.dir,
                                              "selfrw.yaml")).parse()
        print prog.deps
        prog.dispose()
        prog = YamlProgramParser(os.path.join(self.dir,
                                              "mm-mm.yaml")).parse()
        print prog.deps
        prog.dispose()
        pass

    def tearDown(self):
        pass
