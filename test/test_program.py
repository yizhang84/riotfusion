'''
Created on Oct 12, 2011

@author: yizhang
'''
from riot.fusion import *
from riot.parser import *
import os
from logging import *
import unittest

class TestProgram(unittest.TestCase):
    
    def setUp(self):
        basicConfig(level=INFO)
        curdir = os.path.dirname(__file__)
        add_lib_path(os.path.join(curdir, "../.."))
        
    def tearDown(self):
        pass
    
    def test_read_write(self):
        ctx = isl_ctx_alloc()
        d = '[n]->{D1[i]:0<=i<=n; D2[i]:0<=i<=n}'
        r = '{D1[i]->a[i]; D2[i]->a[i]}'
        w = '{D1[i]->b[i+3]; D2[i]->b[i+3]}'
        s = '{D1[i]->[0,i]; D2[i]->[1,i]}'
        domain = isl_union_set_read_from_str(ctx, d)
        read = isl_union_map_read_from_str(ctx, r)
        write = isl_union_map_read_from_str(ctx, w)
        schedule = isl_union_map_read_from_str(ctx, s)
        read = isl_union_map_intersect_domain(read,
                                              isl_union_set_copy(domain))
        write = isl_union_map_intersect_domain(write,
                                               isl_union_set_copy(domain))
        schedule = isl_union_map_intersect_domain(schedule,
                                                  isl_union_set_copy(domain))
        prog = Program(ctx, domain, read, write, schedule)
        waw = [x for x in prog.deps if x.type is Type.WAW]
        rar = [x for x in prog.deps if x.type is Type.RAR]
        self.assertEqual(len(waw), 1)
        self.assertEqual(len(rar), 1)
        self.assertTrue(isl_is_equal(waw[0].dep,
                                     '[n]->{D1[i]->D2[i]: i>=0 and i<=n}'))
        self.assertTrue(isl_is_equal(rar[0].dep,
                                     '[n]->{D1[i]->D2[i]: i>=0 and i<=n}'))
        self.assertTrue(isl_is_equal(waw[0].access,
                                     '[n]->{D2[i]->b[i+3]: i>=0 and i<=n}'))
        self.assertTrue(isl_is_equal(rar[0].access,
                                     '[n]->{D2[i]->a[i]: i>=0 and i<=n}'))
        prog.dispose()

    def test_read_read(self):
        ctx = isl_ctx_alloc()
        d = '[n]->{D1[i]:0<=i<=n; D2[i]:0<=i<=n}'
        r = '{D1[i]->a[i]; D2[i]->a[i]; D1[i]->a[i+1]; D2[i]->a[i+1]}'
        w = '{}'
        s = '{D1[i]->[0,i]; D2[i]->[1,i]}'
        domain = isl_union_set_read_from_str(ctx, d)
        read = isl_union_map_read_from_str(ctx, r)
        write = isl_union_map_read_from_str(ctx, w)
        schedule = isl_union_map_read_from_str(ctx, s)
        read = isl_union_map_intersect_domain(read,
                                              isl_union_set_copy(domain))
        write = isl_union_map_intersect_domain(write,
                                               isl_union_set_copy(domain))
        schedule = isl_union_map_intersect_domain(schedule,
                                                  isl_union_set_copy(domain))
        prog = Program(ctx, domain, read, write, schedule)
        waw = [x for x in prog.deps if x.type is Type.WAW]
        rar = [x for x in prog.deps if x.type is Type.RAR]
        self.assertEqual(len(waw), 0)
        self.assertEqual(len(rar), 6)
        ans = set([('[n]->{D1[i]->D1[i+1]:0<=i<n}', '[n]->{D1[i]->a[i]:1<=i<=n}'),
                   ('[n]->{D2[i]->D2[i+1]:0<=i<n}', '[n]->{D2[i]->a[i]:1<=i<=n}'),
                   ('[n]->{D1[i]->D2[i]: 0<=i<=n}', '[n]->{D2[i]->a[i]:0<=i<=n}'),
                   ('[n]->{D1[i]->D2[i]: 0<=i<=n}', '[n]->{D2[i]->a[i+1]:0<=i<=n}'),
                   ('[n]->{D1[i]->D2[i-1]:0<i<=n}', '[n]->{D2[i]->a[i+1]:0<=i<n}'),
                   ('[n]->{D1[i]->D2[i+1]:0<=i<n}', '[n]->{D2[i]->a[i]:1<=i<=n}')])
        matched = set()
        for x in rar:
            for y in ans-matched:
                if (isl_is_equal(x.dep, y[0]) and
                    isl_is_equal(x.access, y[1])):
                    matched.add(y)
                    break
        self.assertEqual(len(rar), len(matched))
        prog.dispose()
